# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Ideaio
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")

    config.middleware.use(Rack::UTF8Sanitizer)

    config.time_zone = 'UTC'
    config.active_record.default_timezone = :utc

    # Use Delayed::Job for ActiveJob backend.
    config.active_job.queue_adapter = :delayed_job

    config.colorize_logging = true
    config.log_tags = { request_id: :request_id }
    config.rails_semantic_logger.format = :json
    config.rails_semantic_logger.semantic   = true
    config.rails_semantic_logger.started    = true
    config.rails_semantic_logger.processing = true
    config.rails_semantic_logger.rendered   = true
    config.rails_semantic_logger.console_logger = true
    config.rails_semantic_logger.quiet_assets = true
  end
end
