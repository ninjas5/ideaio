# frozen_string_literal: true

Sentry.init do |config|
  # TODO: Now, using the ideaio project in sky's developer account, we can
  # easily send event to other projects just by modifying this url.
  config.dsn = 'https://ff7ef98edb7d411ea4b3b42b5c290975@o1277300.ingest.sentry.io/6474722'
  config.breadcrumbs_logger = %i[active_support_logger http_logger]

  # Set tracesSampleRate to 1.0 to capture 100%
  # of transactions for performance monitoring.
  # We recommend adjusting this value in production
  config.traces_sample_rate = 1.0
  # config.traces_sampler = lambda do |context|
  #   true
  # end
end
