# frozen_string_literal: true

Slack::Web::Client.configure do |config|
  config.logger = Rails.logger
  config.timeout = 3000
  config.open_timeout = 3000
end
