# frozen_string_literal: true

Rails.application.routes.draw do
  use_doorkeeper

  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'

  resources :home, only: %i[index]
  get 'ping' => 'home#ping'

  namespace :api do
    namespace :v1 do
      resources :configs, only: %i[index]
      resources :workspaces, only: %i[index]
      resources :idea_templates, only: %i[index show]
      resources :idea_definitions, only: %i[create destroy index show update]
      resources :idea_reports, only: %i[index show]
      resources :ideas, only: %i[index show] do
        member do
          post :answer
        end
      end
      resources :question_answers, only: [] do
        member do
          post :vote
        end

        resources :answer_comments, only: %i[create destroy], path: 'comments'
      end
      # Slack login
      get 'login', to: 'slack#login'
      post 'logout', to: 'slack#logout'

      # Notion connect
      get 'integration/notion/connect', to: 'notion#connect'
      get 'integration/notion/callback', to: 'notion#callback'
      post 'integration/notion/disconnect', to: 'notion#disconnect'
      post 'integration/notion/export', to: 'notion#export'
      get 'integration/notion/dblist', to: 'notion#dblist'
      get 'integration/notion/check', to: 'notion#check'

      resources :slack, only: [] do
        collection do
          get :oauth_authorize
        end
      end

      resources :settings, only: [] do
        collection do
          get :timezones
        end
      end

      namespace :slack do
        resources :channels, only: %i[index]
        resources :users, only: %i[index]
      end

      get 'profile', to: 'users#profile'

      resources :users, only: [] do
        collection do
          post :archive

          # TODO: 只是为了方便开发测试
          get :access_tokens unless Rails.env.production?
        end
      end
    end
  end

  root 'home#index'
end
