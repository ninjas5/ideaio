# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_04_30_124239) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "answer_comments", force: :cascade do |t|
    t.bigint "workspace_id", null: false
    t.bigint "user_id", null: false
    t.bigint "question_answer_id", null: false
    t.string "content", limit: 512, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["question_answer_id"], name: "index_answer_comments_on_question_answer_id"
    t.index ["user_id"], name: "index_answer_comments_on_user_id"
    t.index ["workspace_id"], name: "index_answer_comments_on_workspace_id"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["priority", "run_at"], name: "index_delayed_jobs_on_priority_and_run_at"
  end

  create_table "idea_definitions", force: :cascade do |t|
    t.bigint "workspace_id", null: false
    t.bigint "user_id", null: false
    t.bigint "idea_template_id"
    t.string "name", limit: 100, null: false
    t.string "description", limit: 255, default: "", null: false
    t.string "status", limit: 32, null: false
    t.boolean "repeatable", null: false
    t.jsonb "timer", null: false
    t.string "timezone", limit: 32, null: false
    t.jsonb "channels", null: false
    t.jsonb "invited_participants", null: false
    t.boolean "report_shareable", null: false
    t.datetime "archived_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "anonymous"
    t.index ["idea_template_id"], name: "index_idea_definitions_on_idea_template_id"
    t.index ["user_id"], name: "index_idea_definitions_on_user_id"
    t.index ["workspace_id"], name: "index_idea_definitions_on_workspace_id"
  end

  create_table "idea_participants", force: :cascade do |t|
    t.bigint "workspace_id", null: false
    t.bigint "user_id", null: false
    t.bigint "idea_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["idea_id", "user_id"], name: "index_idea_participants_on_idea_id_and_user_id", unique: true
    t.index ["idea_id"], name: "index_idea_participants_on_idea_id"
    t.index ["user_id"], name: "index_idea_participants_on_user_id"
    t.index ["workspace_id"], name: "index_idea_participants_on_workspace_id"
  end

  create_table "idea_question_definitions", force: :cascade do |t|
    t.bigint "idea_definition_id", null: false
    t.integer "ordinal", null: false
    t.string "content", limit: 512, null: false
    t.boolean "required", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["idea_definition_id"], name: "index_idea_question_definitions_on_idea_definition_id"
  end

  create_table "idea_questions", force: :cascade do |t|
    t.bigint "idea_id", null: false
    t.integer "ordinal", null: false
    t.string "content", limit: 512, null: false
    t.boolean "required", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["idea_id"], name: "index_idea_questions_on_idea_id"
  end

  create_table "idea_template_questions", force: :cascade do |t|
    t.bigint "idea_template_id", null: false
    t.integer "ordinal", null: false
    t.string "content", limit: 512, null: false
    t.boolean "required", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["idea_template_id"], name: "index_idea_template_questions_on_idea_template_id"
    t.index ["ordinal", "idea_template_id"], name: "index_idea_template_questions_on_ordinal_and_idea_template_id"
  end

  create_table "idea_templates", force: :cascade do |t|
    t.bigint "workspace_id"
    t.bigint "user_id"
    t.string "category", limit: 64, null: false
    t.string "name", limit: 100, null: false
    t.string "description", limit: 255, default: "", null: false
    t.string "status", limit: 32, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "user_id"], name: "index_idea_templates_on_name_and_user_id", unique: true
    t.index ["name", "workspace_id"], name: "index_idea_templates_on_name_and_workspace_id", unique: true
    t.index ["user_id"], name: "index_idea_templates_on_user_id"
    t.index ["workspace_id"], name: "index_idea_templates_on_workspace_id"
  end

  create_table "ideas", force: :cascade do |t|
    t.bigint "workspace_id", null: false
    t.bigint "idea_definition_id", null: false
    t.string "name", limit: 128, null: false
    t.string "description", limit: 255, default: "", null: false
    t.datetime "start_time", null: false
    t.datetime "release_time", null: false
    t.string "timezone", limit: 32, null: false
    t.jsonb "invited_participants", null: false
    t.boolean "report_shareable", null: false
    t.string "status", limit: 32, null: false
    t.string "running_status", limit: 32, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "anonymous"
    t.index ["idea_definition_id"], name: "index_ideas_on_idea_definition_id"
    t.index ["workspace_id"], name: "index_ideas_on_workspace_id"
  end

  create_table "notion_connections", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "token_type", limit: 255
    t.string "access_token", limit: 255
    t.string "bot_id", limit: 255
    t.string "workspace_name", limit: 255
    t.string "workspace_icon"
    t.string "workspace_id"
    t.jsonb "owner", default: {}, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["access_token"], name: "index_notion_connections_on_access_token"
    t.index ["bot_id"], name: "index_notion_connections_on_bot_id", unique: true
    t.index ["user_id"], name: "index_notion_connections_on_user_id"
  end

  create_table "oauth_access_grants", force: :cascade do |t|
    t.bigint "resource_owner_id", null: false
    t.bigint "application_id", null: false
    t.string "token", null: false
    t.integer "expires_in", null: false
    t.text "redirect_uri", null: false
    t.datetime "created_at", null: false
    t.datetime "revoked_at"
    t.string "scopes", default: "", null: false
    t.index ["application_id"], name: "index_oauth_access_grants_on_application_id"
    t.index ["resource_owner_id"], name: "index_oauth_access_grants_on_resource_owner_id"
    t.index ["token"], name: "index_oauth_access_grants_on_token", unique: true
  end

  create_table "oauth_access_tokens", force: :cascade do |t|
    t.bigint "resource_owner_id"
    t.bigint "application_id", null: false
    t.string "token", null: false
    t.string "refresh_token"
    t.integer "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at", null: false
    t.string "scopes"
    t.string "previous_refresh_token", default: "", null: false
    t.index ["application_id"], name: "index_oauth_access_tokens_on_application_id"
    t.index ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true
    t.index ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id"
    t.index ["token"], name: "index_oauth_access_tokens_on_token", unique: true
  end

  create_table "oauth_applications", force: :cascade do |t|
    t.string "name", null: false
    t.string "uid", null: false
    t.string "secret", null: false
    t.text "redirect_uri", null: false
    t.string "scopes", default: "", null: false
    t.boolean "confidential", default: true, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["uid"], name: "index_oauth_applications_on_uid", unique: true
  end

  create_table "question_answers", force: :cascade do |t|
    t.bigint "workspace_id", null: false
    t.bigint "idea_participant_id", null: false
    t.bigint "idea_question_id", null: false
    t.string "content", limit: 2048, default: "", null: false
    t.jsonb "tags", default: [], null: false
    t.jsonb "attachments", default: [], null: false
    t.jsonb "vote_user_ids", default: [], null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["idea_participant_id", "idea_question_id"], name: "uk_question_answers_on_participant_n_question", unique: true
    t.index ["idea_participant_id"], name: "index_question_answers_on_idea_participant_id"
    t.index ["idea_question_id"], name: "index_question_answers_on_idea_question_id"
    t.index ["workspace_id"], name: "index_question_answers_on_workspace_id"
  end

  create_table "slack_teams", force: :cascade do |t|
    t.bigint "workspace_id", null: false
    t.string "ref_team_id", limit: 255, null: false
    t.string "ref_team_name", limit: 255, null: false
    t.string "scope", limit: 255, default: "", null: false
    t.string "access_token", limit: 255
    t.string "refresh_token", limit: 255
    t.integer "expires_in"
    t.datetime "expires_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["access_token"], name: "index_slack_teams_on_access_token", unique: true
    t.index ["refresh_token"], name: "index_slack_teams_on_refresh_token", unique: true
    t.index ["workspace_id"], name: "index_slack_teams_on_workspace_id"
    t.index ["workspace_id"], name: "uk_slack_teams_on_workspace", unique: true
  end

  create_table "slack_users", force: :cascade do |t|
    t.bigint "workspace_id", null: false
    t.bigint "slack_team_id", null: false
    t.bigint "user_id", null: false
    t.string "ref_user_id", limit: 255, null: false
    t.string "email", limit: 255, null: false
    t.string "scope", limit: 255, default: "", null: false
    t.string "token_type", limit: 255
    t.string "access_token", limit: 255
    t.string "refresh_token", limit: 255
    t.integer "expires_in"
    t.datetime "expires_at"
    t.jsonb "profile", default: {}, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["access_token"], name: "index_slack_users_on_access_token"
    t.index ["refresh_token"], name: "index_slack_users_on_refresh_token"
    t.index ["slack_team_id"], name: "index_slack_users_on_slack_team_id"
    t.index ["user_id"], name: "index_slack_users_on_user_id"
    t.index ["user_id"], name: "uk_slack_users_on_user", unique: true
    t.index ["workspace_id"], name: "index_slack_users_on_workspace_id"
  end

  create_table "users", force: :cascade do |t|
    t.bigint "workspace_id", null: false
    t.string "email", limit: 255, null: false
    t.jsonb "roles", null: false
    t.datetime "archived_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email"
    t.index ["workspace_id"], name: "index_users_on_workspace_id"
  end

  create_table "workspaces", force: :cascade do |t|
    t.string "category", limit: 64, null: false
    t.string "name", limit: 255, null: false
    t.datetime "archived_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "category"], name: "index_workspaces_on_name_and_category", unique: true
  end

  add_foreign_key "answer_comments", "question_answers"
  add_foreign_key "answer_comments", "users"
  add_foreign_key "answer_comments", "workspaces"
  add_foreign_key "idea_definitions", "idea_templates"
  add_foreign_key "idea_definitions", "users"
  add_foreign_key "idea_definitions", "workspaces"
  add_foreign_key "idea_participants", "ideas"
  add_foreign_key "idea_participants", "users"
  add_foreign_key "idea_participants", "workspaces"
  add_foreign_key "idea_question_definitions", "idea_definitions"
  add_foreign_key "idea_questions", "ideas"
  add_foreign_key "idea_template_questions", "idea_templates"
  add_foreign_key "idea_templates", "users"
  add_foreign_key "idea_templates", "workspaces"
  add_foreign_key "ideas", "idea_definitions"
  add_foreign_key "ideas", "workspaces"
  add_foreign_key "notion_connections", "users"
  add_foreign_key "oauth_access_grants", "oauth_applications", column: "application_id"
  add_foreign_key "oauth_access_grants", "users", column: "resource_owner_id"
  add_foreign_key "oauth_access_tokens", "oauth_applications", column: "application_id"
  add_foreign_key "oauth_access_tokens", "users", column: "resource_owner_id"
  add_foreign_key "question_answers", "idea_participants"
  add_foreign_key "question_answers", "idea_questions"
  add_foreign_key "question_answers", "workspaces"
  add_foreign_key "slack_teams", "workspaces"
  add_foreign_key "slack_users", "slack_teams"
  add_foreign_key "slack_users", "users"
  add_foreign_key "slack_users", "workspaces"
  add_foreign_key "users", "workspaces"
end
