# frozen_string_literal: true

class InitOauthApplication < ActiveRecord::Migration[6.1]
  def up
    return if Doorkeeper::Application.exists?(name: CommonConst::OAuthApplication::IDEAIO)

    Doorkeeper::Application.create!(
      name: CommonConst::OAuthApplication::IDEAIO,
      redirect_uri: 'urn:ietf:wg:oauth:2.0:oob',
      scopes: %w[read write],
    )
  end
end
