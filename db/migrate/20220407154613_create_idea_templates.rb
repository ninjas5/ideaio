# frozen_string_literal: true

class CreateIdeaTemplates < ActiveRecord::Migration[6.1]
  def change
    create_table :idea_templates do |t|
      t.references  :workspace,   null: true,   foreign_key: true
      t.references  :user,        null: true,   foreign_key: true
      t.string      :category,    null: false,  limit: 64
      t.string      :name,        null: false,  limit: 100
      t.string      :description, null: false,  limit: 255, default: ''
      t.string      :status,      null: false,  limit: 32
      t.timestamps                null: false
    end

    add_index :idea_templates, %i[name workspace_id], unique: true
    add_index :idea_templates, %i[name user_id], unique: true
  end
end
