# frozen_string_literal: true

class CreateIdeaParticipants < ActiveRecord::Migration[6.1]
  def change
    create_table :idea_participants do |t|
      t.references  :workspace, null: false,  foreign_key: true
      t.references  :user,      null: false,  foreign_key: true
      t.references  :idea,      null: false,  foreign_key: true
      t.timestamps              null: false
    end

    add_index :idea_participants, %i[idea_id user_id], unique: true
  end
end
