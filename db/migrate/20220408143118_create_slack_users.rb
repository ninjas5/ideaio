# frozen_string_literal: true

class CreateSlackUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :slack_users do |t|
      t.references  :workspace,     null: false,  foreign_key: true
      t.references  :slack_team,    null: false,  foreign_key: true
      t.references  :user,          null: false,  foreign_key: true
      t.string      :ref_user_id,   null: false,  limit: 255
      t.string      :email,         null: false,  limit: 255
      t.string      :scope,         null: false,  limit: 255, default: ''
      t.string      :token_type,    null: true,   limit: 255
      t.string      :access_token,  null: true,   limit: 255
      t.string      :refresh_token, null: true,   limit: 255
      t.integer     :expires_in,    null: true
      t.datetime    :expires_at,    null: true
      t.jsonb       :profile,       null: false, default: {}
      t.timestamps                  null: false
    end

    add_index :slack_users, :user_id, unique: true, name: :uk_slack_users_on_user
    add_index :slack_users, :access_token
    add_index :slack_users, :refresh_token
  end
end
