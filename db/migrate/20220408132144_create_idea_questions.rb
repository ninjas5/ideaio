# frozen_string_literal: true

class CreateIdeaQuestions < ActiveRecord::Migration[6.1]
  def change
    create_table :idea_questions do |t|
      t.references  :idea,      null: false, foreign_key: true
      t.integer     :ordinal,   null: false
      t.string      :content,   null: false, limit: 512
      t.boolean     :required,  null: false
      t.timestamps              null: false
    end
  end
end
