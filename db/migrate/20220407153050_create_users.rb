# frozen_string_literal: true

class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.references  :workspace,   null: false,  foreign_key: true
      t.string      :email,       null: false,  limit: 255
      t.jsonb       :roles,       null: false
      t.datetime    :archived_at, null: true
      t.timestamps                null: false
    end

    add_index :users, :email
  end
end
