# frozen_string_literal: true

class CreateSlackTeams < ActiveRecord::Migration[6.1]
  def change
    create_table :slack_teams do |t|
      t.references  :workspace,     null: false,  foreign_key: true
      t.string      :ref_team_id,   null: false,  limit: 255
      t.string      :ref_team_name, null: false,  limit: 255
      t.string      :scope,         null: false,  limit: 255, default: ''
      t.string      :access_token,  null: true,   limit: 255
      t.string      :refresh_token, null: true,   limit: 255
      t.integer     :expires_in,    null: true
      t.datetime    :expires_at,    null: true
      t.timestamps                  null: false
    end

    add_index :slack_teams, :workspace_id, unique: true, name: :uk_slack_teams_on_workspace
    add_index :slack_teams, :access_token, unique: true
    add_index :slack_teams, :refresh_token, unique: true
  end
end
