# frozen_string_literal: true

class CreateIdeaTemplateQuestions < ActiveRecord::Migration[6.1]
  def change
    create_table :idea_template_questions do |t|
      t.references  :idea_template, null: false,  foreign_key: true
      t.integer     :ordinal,       null: false,  limit: 4
      t.string      :content,       null: false,  limit: 512
      t.boolean     :required,      null: false,  default: false
      t.timestamps                  null: false
    end

    add_index :idea_template_questions, %i[ordinal idea_template_id]
  end
end
