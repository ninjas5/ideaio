# frozen_string_literal: true

class CreateWorkspaces < ActiveRecord::Migration[6.1]
  def change
    create_table :workspaces do |t|
      t.string    :category,    null: false,  limit: 64
      t.string    :name,        null: false,  limit: 255
      t.datetime  :archived_at, null: true
      t.timestamps              null: false
    end

    add_index :workspaces, %i[name category], unique: true
  end
end
