# frozen_string_literal: true

class CreateIdeas < ActiveRecord::Migration[6.1]
  def change
    create_table :ideas do |t|
      t.references  :workspace,             null: false,  foreign_key: true
      t.references  :idea_definition,       null: false,  foreign_key: true
      t.string      :name,                  null: false,  limit: 128
      t.string      :description,           null: false,  limit: 255, default: ''
      t.datetime    :start_time,            null: false
      t.datetime    :release_time,          null: false
      t.string      :timezone,              null: false, limit: 32
      t.jsonb       :invited_participants,  null: false
      t.boolean     :report_shareable,      null: false
      t.string      :status,                null: false,  limit: 32
      t.string      :running_status,        null: false,  limit: 32
      t.timestamps                          null: false
    end
  end
end
