# frozen_string_literal: true

class AddAnonymityToIdeaDefinition < ActiveRecord::Migration[6.1]
  def change
    add_column :idea_definitions, :anonymous, :boolean
    add_column :ideas, :anonymous, :boolean
  end
end
