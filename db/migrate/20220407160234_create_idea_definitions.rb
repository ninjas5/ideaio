# frozen_string_literal: true

class CreateIdeaDefinitions < ActiveRecord::Migration[6.1]
  def change
    create_table :idea_definitions do |t|
      t.references  :workspace,             null: false,  foreign_key: true
      t.references  :user,                  null: false,  foreign_key: true
      t.references  :idea_template,         null: true,   foreign_key: true
      t.string      :name,                  null: false,  limit: 100
      t.string      :description,           null: false,  limit: 255, default: ''
      t.string      :status,                null: false,  limit: 32
      t.boolean     :repeatable,            null: false
      t.jsonb       :timer,                 null: false
      t.string      :timezone,              null: false, limit: 32
      t.jsonb       :channels,              null: false
      t.jsonb       :invited_participants,  null: false
      t.boolean     :report_shareable,      null: false
      t.datetime    :archived_at,           null: true
      t.timestamps                          null: false
    end
  end
end
