# frozen_string_literal: true

class CreateAnswerComments < ActiveRecord::Migration[6.1]
  def change
    create_table :answer_comments do |t|
      t.references  :workspace,       null: false,  foreign_key: true
      t.references  :user,            null: false,  foreign_key: true
      t.references  :question_answer, null: false,  foreign_key: true
      t.string      :content,         null: false,  limit: 512
      t.timestamps                    null: false
    end
  end
end
