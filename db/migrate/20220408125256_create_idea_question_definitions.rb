# frozen_string_literal: true

class CreateIdeaQuestionDefinitions < ActiveRecord::Migration[6.1]
  def change
    create_table :idea_question_definitions do |t|
      t.references  :idea_definition, null: false, foreign_key: true
      t.integer     :ordinal,         null: false
      t.string      :content,         null: false,  limit: 512
      t.boolean     :required,        null: false,  default: false
      t.timestamps                    null: false
    end
  end
end
