# frozen_string_literal: true

class CreateNotionConnections < ActiveRecord::Migration[6.1]
  def change
    create_table :notion_connections do |t|
      t.references  :user,          null: false,  foreign_key: true
      t.string      :token_type,    null: true,   limit: 255
      t.string      :access_token,  null: true,   limit: 255
      t.string      :bot_id, null: true, limit: 255
      t.string      :workspace_name, null: true, limit: 255
      t.string      :workspace_icon, null: true
      t.string      :workspace_id, null: true
      t.jsonb       :owner, null: false, default: {}
      t.timestamps null: false
    end

    add_index :notion_connections, :bot_id, unique: true
    add_index :notion_connections, :access_token
  end
end
