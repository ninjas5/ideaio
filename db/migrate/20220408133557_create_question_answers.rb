# frozen_string_literal: true

class CreateQuestionAnswers < ActiveRecord::Migration[6.1]
  def change
    create_table :question_answers do |t|
      t.references  :workspace,         null: false,  foreign_key: true
      t.references  :idea_participant,  null: false,  foreign_key: true
      t.references  :idea_question,     null: false,  foreign_key: true
      t.string      :content,           null: false,  limit: 2048, default: ''
      t.jsonb       :tags,              null: false,  default: []
      t.jsonb       :attachments,       null: false,  default: []
      t.jsonb       :vote_user_ids,     null: false,  default: []
      t.timestamps                      null: false
    end

    add_index :question_answers, %i[idea_participant_id idea_question_id], unique: true,
      name: :uk_question_answers_on_participant_n_question
  end
end
