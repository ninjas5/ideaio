# frozen_string_literal: true

# TODO: sky.wang 很快会弃用这种方式设置数据
module Seeds
  class << self
    def setup
      ApplicationRecord.transaction do
        cleanup

        init_data
      end
    end

    private

    def cleanup
      AnswerComment.destroy_all
      QuestionAnswer.destroy_all
      IdeaParticipant.destroy_all

      IdeaQuestion.destroy_all
      Idea.destroy_all

      IdeaQuestionDefinition.destroy_all
      IdeaDefinition.destroy_all

      IdeaTemplateQuestion.destroy_all
      IdeaTemplate.destroy_all

      SlackUser.destroy_all
      SlackTeam.destroy_all

      User.destroy_all
      Workspace.destroy_all
    end

    def init_data
      workspace = Workspace.create!(category: Workspace::Category::SLACK, name: 'test')
      sky, = users = workspace.users.create!([
        { email: 'sky@test.com', roles: [User::Role::ADMIN, User::Role::BASIC] },
        { email: 'emma@test.com', roles: [User::Role::ADMIN, User::Role::BASIC] },
        { email: 'test@test.com', roles: [User::Role::BASIC] },
      ])

      slack_team = workspace.create_slack_team!(
        ref_team_id: 'slack_team_test_1',
        ref_team_name: workspace.name,
        bot_token: "#{workspace.id}-bot-token",
      )

      slack_users_params = users.map do |user|
        {
          workspace: slack_team.workspace,
          user: user,
          ref_user_id: "slack_user_id_#{user.id}",
          name: "user name #{user.id}",
          nickname: "user nickname #{user.id}",
          email: user.email,
          picture_url: '',
          access_token: "access_token_#{user.id}",
        }
      end
      slack_team.slack_users.create!(slack_users_params)

      # Global Template - Daily standup
      daily_standup = IdeaTemplate.create!(
        category: IdeaTemplate::Category::GENERAL,
        name: 'Daily standup',
        description: 'daily standup',
        status: IdeaTemplate::Status::ENABLED,
      )
      daily_standup.questions.create!([
        { ordinal: 0, required: true, content: '昨天完成了什么？' },
        { ordinal: 1, required: true, content: '今天计划做什么？' },
        { ordinal: 2, required: true, content: '有什么被block了么？' },
      ])

      # Global Template - Sprint retro
      retro = IdeaTemplate.create!(
        category: IdeaTemplate::Category::GENERAL,
        name: 'Retrospective',
        description: 'Sprint Retrospective',
        status: IdeaTemplate::Status::ENABLED,
      )
      retro.questions.create!([
        { ordinal: 0, required: true, content: '有哪些做得好的？' },
        { ordinal: 1, required: true, content: '有哪些需要改善的？' },
        { ordinal: 2, required: true, content: '工作量多少？' },
      ])

      # Idea definition
      daily_standup_definition = daily_standup.idea_definitions.create!(
        workspace: workspace,
        user: sky,
        name: daily_standup.name,
        description: daily_standup.description,
        status: IdeaDefinition::Status::ENABLED,
        repeatable: true,
        timezone: CommonConst::ORIGIN_TIMEZONES.first,
        timer: {
          cycle: '1.weeks',
          triggers: [
            { ordinal: 0, start_time: '09:00:00', period_time: '30.minutes' },
            { ordinal: 1, start_time: '09:00:00', period_time: '30.minutes' },
            { ordinal: 2, start_time: '09:00:00', period_time: '30.minutes' },
            { ordinal: 3, start_time: '09:00:00', period_time: '30.minutes' },
            { ordinal: 4, start_time: '09:00:00', period_time: '30.minutes' },
          ],
        },
        channels: ['general'],
        invited_participants: sky.workspace.slack_users.map do |slack_user|
          {
            category: sky.workspace.category,
            ref_team_id: slack_user.slack_team.ref_team_id,
            ref_user_id: slack_user.ref_user_id,
          }
        end,
        report_shareable: true,
      )
      daily_standup_definition.questions.create!(
        daily_standup.questions.map do |question|
          question.slice(:ordinal, :required, :content)
        end,
      )

      # Idea instance
      keys = %i[workspace name description timezone invited_participants report_shareable status]
      idea = daily_standup_definition.ideas.create!(
        daily_standup_definition.slice(*keys)
          .merge(
            start_time: Time.zone.parse('2022-04-09 09:00:00'),
            release_time: Time.zone.parse('2022-04-09 09:30:00'),
            running_status: Idea::RunningStatus::RELEASED,
          ),
      )
      idea.questions.create!(
        daily_standup_definition.questions.map do |question|
          question.slice(:ordinal, :required, :content)
        end,
      )

      idea_participants = idea.idea_participants.create!(
        idea.invited_participants.map do |participant|
          temp_team = SlackTeam.find_by!(ref_team_id: participant[:ref_team_id])
          slack_user = temp_team.slack_users.find_by!(ref_user_id: participant[:ref_user_id])

          {
            workspace: temp_team.workspace,
            user: slack_user.user,
          }
        end,
      )

      idea_participants.each do |idea_participant|
        idea_participant.question_answers.create!(
          idea_participant.idea.questions.map do |question|
            {
              workspace: idea_participant.workspace,
              idea_question: question,
              content: "Answer-#{question.ordinal}, #{question.content}",
              tags: [],
              attachments: [],
              vote_user_ids: [sky.id],
            }
          end,
        )
      end
    end
  end
end

Seeds.setup
