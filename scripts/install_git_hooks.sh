#!/bin/bash

gitroot=$(git rev-parse --show-toplevel || echo ".")
cp $gitroot/scripts/git_hooks/pre-commit.sample $gitroot/.git/hooks/pre-commit
chmod u+x $gitroot/.git/hooks/pre-commit
