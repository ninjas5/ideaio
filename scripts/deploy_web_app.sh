#!/bin/bash

TAG=`date +'%Y%m%d%H%M%S'`

function show_helper() {
    echo "Syntax: $0 <environment>"
    echo "Parameters:"
    echo "    1. environment: default: \"production\", values: [\"staging\", \"production\"]"
    echo "Received invalid parameters. \"$0 $*\""
}

function build_assets() {
    npm run build
}

function deploy_to_remote() {
    local host=$1
    local port=$2

    echo "Deploy to ${host}:${port}, start ... ..."
    local target="releases/${TAG}"

    build_assets && \
        echo "Build assets successful." && \
        mkdir -p releases && \
        cd dist/ && \
        tar -czf ../${target}.tgz * && \
        cd - && \
        scp -rC -P ${port} ${target}.tgz app@${host}:/opt/app/ideaio_app/ && \
        ssh app@${host} -p ${port} -C "
            cd /opt/app/ideaio_app && \\
            mkdir -p ${target} && \\
            tar -xzf ${TAG}.tgz -C ${target} && \\
            rm -f current ${TAG}.tgz && \\
            ln -sf ${target} current && \\
            ls releases | sort | head -n -3 | sed 's/^/releases\//g' | xargs -n 1 rm -fr && \\
            echo 'Deploy assets successful.'
        " && \
        echo "Deploy to ${host}:${port}, successful."
}

function deploy_web_app() {
    cd $(git rev-parse --show-toplevel || pwd)

    if [ "${2}" != "" ]
    then
        show_helper $*
        return 1
    fi

    case ${1} in
    "production")
        deploy_to_remote 143.198.239.195 60022
        ;;
    "staging")
        deploy_to_remote 35.220.169.8 22
        ;;
    *)
        show_helper $*
        return 1
        ;;
    esac
}

function main() {
    local current_path=$(pwd)

    # TODO: 修改这个变量为前端项目所在的路径（相对路径、绝对路径都可以）
    # local ideaio_frontend_path="../ideaio-fe"
    local ideaio_frontend_path="/Users/emmaguo/Documents/code/ideaio-frontend/ideaio-fe"
    cd ${ideaio_frontend_path}

    deploy_web_app $*

    cd ${current_path}
}

main $@
