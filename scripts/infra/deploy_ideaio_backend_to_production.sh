#!/bin/bash

function delete_old() {
    docker rm -f ideaio_production
}

function deploy_new() {
    docker pull registry.digitalocean.com/ideaio/ideaio && \
        docker run --name ideaio_production --add-host=host.docker.internal:host-gateway \
        -m 2048M --memory-swap -1 -p 10000:80 --env-file=.ideaio_prd_env -itd \
        -v /opt/app/ideaio/log:/app/log \
        registry.digitalocean.com/ideaio/ideaio
}

function remove_unused_images() {
    docker images | grep registry.digitalocean.com/ideaio/ideaio | \
        grep -v latest | awk -F ' ' '{print $3}' | xargs -n 1 docker rmi
}

function main() {
    delete_old
    deploy_new && remove_unused_images
}

main
