#!/bin/bash

function delete_old() {
    docker rm -f ideaio_staging
}

function deploy_new() {
  docker pull us-docker.pkg.dev/valid-heuristic-346114/ideaio/ideaio-staging && \
    docker run --name ideaio_staging --add-host=host.docker.internal:host-gateway \
      -m 1024M --memory-swap -1 -p 10001:80 --env-file=.ideaio_staging_env -itd \
      -v /opt/app/ideaio/log:/app/log \
      us-docker.pkg.dev/valid-heuristic-346114/ideaio/ideaio-staging
}

function remove_unused_images() {
    docker images | grep us-docker.pkg.dev/valid-heuristic-346114/ideaio/ideaio-staging | \
        grep -v latest | awk -F ' ' '{print $3}' | xargs -n 1 docker rmi -f
}

function main() {
    delete_old
    deploy_new && remove_unused_images
}

main
