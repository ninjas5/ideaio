#!/bin/bash

# Load the parameters from config file for the Space you want to upload to.
source ~/.ideaio_prd_env

# Uploads your object via cURL.
function put_file_to_space()
{
  local filepath=${1}
  local space_path=${2} # The path within your Space where you want to upload the new file.
  local space="${DIGITAL_OCEAN_PRODUCTION_SPACE}"
  local content_type="application/x-gzip" # Defines the type of content you are uploading.
  local date=$(date +"%a, %d %b %Y %T %z")
  local acl="x-amz-acl:private" # Defines Access-control List (ACL) permissions, such as private or public.
  local storage_type="x-amz-storage-class:${DIGITAL_OCEAN_PRODUCTION_SPACE_STORAGE_TYPE}"
  local src_string="PUT\n\n${content_type}\n${date}\n${acl}\n${storage_type}\n/${space}${space_path}"
  local signature=$(echo -en "${src_string}" | openssl sha1 -hmac "${DIGITAL_OCEAN_PRODUCTION_SPACE_SECRET}" -binary | base64)
  local domain="${space}.${DIGITAL_OCEAN_PRODUCTION_SPACE_REGION}.digitaloceanspaces.com"

  # The cURL command that uploads your file.
  curl -s -X PUT -T "${filepath}" \
    -H "Host: ${domain}" \
    -H "Content-Type: ${content_type}" \
    -H "Date: ${date}" \
    -H "${storage_type}" \
    -H "${acl}" \
    -H "Authorization: AWS ${DIGITAL_OCEAN_PRODUCTION_SPACE_KEY}:${signature}" \
    "https://${domain}${space_path}"
}

function dump_ideaio_production_db() {
  local target_file=${1}
  local port="${IDEAIO_PRODUCTION_DATABASE_PORT}"
  local user="postgres"
  local password="${IDEAIO_PRODUCTION_DATABASE_PASSWORD}"
  local database="ideaio_production"

  PGPASSWORD=${password} pg_dump -p ${port} -U ${user} ${database} | gzip > ${target_file}
}

function main() {
  local current_path=$(pwd)
  local root_path="/tmp/ideaio/database/full_backup"

  # Step 1: Initialize the temporary file directory
  local version=$(date +'%Y%m%d%H%M%S')
  local target_path="${root_path}/${version}"
  mkdir -p ${target_path} && cd ${target_path}

  # Step 2: Export data from postgresql
  local filename="${version}.psql.gz"
  dump_ideaio_production_db ${filename}

  # Step 4: Run the put_file_to_space function.
  put_file_to_space ${filename} "/database/full_backup/${filename}"

  # Step 5: Delete redundant temporary files
  ls ${root_path} | sort | head -n -3 | xargs -n 1 rm -fr

  cd ${current_path}
}

main

# Run backup job in ideaio production env
# ➜  ~ crontab -l
#   0 3 * * * /bin/bash -l ~/tools/backup_production_db_to_digital_ocean.sh
