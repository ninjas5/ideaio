const displayTime = (time, timezone) => {
    const options = { year: 'numeric', month: 'short', day: '2-digit', hour: '2-digit', minute: 'numeric' }
    if (timezone) {
        options.timeZone = timezone
    }

    return new Date(time).toLocaleString('en-US', options)
}

// Testing
datetime = '2021-12-03T13:59:59z'
// timezone = 'Asia/Chongqing'
// timezone = 'Pacific/Pago_Pago'
// timezone = 'Pacific/Auckland'
// timezone = 'Asia/Karachi'
// timezone = null
console.info(datetime)
console.info(displayTime(datetime, 'Asia/Chongqing'))
console.info(displayTime(datetime, 'Pacific/Pago_Pago'))
console.info(displayTime(datetime, 'Pacific/Auckland'))
console.info(displayTime(datetime, 'Asia/Karachi'))
console.info(displayTime(datetime, null))
