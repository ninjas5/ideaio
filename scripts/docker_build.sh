#!/bin/bash

GITROOT=$(git rev-parse --show-toplevel || pwd)
cd ${GITROOT}

GITCOMMITS=$(git rev-parse HEAD)

DOCKER_REPO="registry.digitalocean.com/ideaio/ideaio"
STAGING_REPO="us-docker.pkg.dev/valid-heuristic-346114/ideaio/ideaio-staging"
TAG=`date +'%Y%m%d%H%M%S'`
export RAILS_ENV="production"

function set_version() {
    echo ${GITCOMMITS} > ${GITROOT}/.gitcommits
}

function compile_rails_assets() {
    rm -fr public/packs* && \
        bundle exec rails webpacker:clobber && \
        bundle exec rails webpacker:compile
}

# Compile rails assets
function compile_assets() {
    compile_rails_assets
}

function generate_api_docs() {
    RAILS_ENV=development bundle exec rails rswag:specs:swaggerize
}

function staging_build() {
    echo "Build staging docker image, start ... ..."

    compile_assets && \
        generate_api_docs && \
        docker build -f infra/staging/Dockerfile -t ${STAGING_REPO}:${TAG} -t ${STAGING_REPO} . && \
        docker push ${STAGING_REPO} && \
        docker push ${STAGING_REPO}:${TAG} && \
        echo "Build staging docker successful."
}

function production_build() {
    echo "Build production docker image, start ... ..."

    compile_assets && \
        generate_api_docs && \
        docker build -f infra/production/Dockerfile -t ${DOCKER_REPO}:${TAG} . && \
        echo "Build docker image successful."
}

function production_build_n_push() {
    echo "Build production docker image and push, start ... ..."

    # Build docker image && push to ali docker registry
    compile_assets && \
        docker build -f infra/production/Dockerfile -t ${DOCKER_REPO}:${TAG} -t ${DOCKER_REPO} . && \
        docker push ${DOCKER_REPO}:${TAG} && \
        docker push ${DOCKER_REPO} && \
        echo "Build docker image and push to docker registry successful."
}

function build_base_production_image() {
    echo "Build base production docker image, start ... ..."

    rm -fr public/packs* && \
        docker build -f infra/production/Dockerfile.base -t ${DOCKER_REPO}:base-${TAG} -t ${DOCKER_REPO}:base . && \
        docker push ${DOCKER_REPO}:base-${TAG} && \
        docker push ${DOCKER_REPO}:base && \
        echo "Build docker image successful."
}

function build_base_staging_image() {
    echo "Build base staging docker image, start ... ..."

    rm -fr public/packs* && \
        docker build -f infra/staging/Dockerfile.base -t ${STAGING_REPO}:base-${TAG} -t ${STAGING_REPO}:base . && \
        docker push ${STAGING_REPO}:base-${TAG} && \
        docker push ${STAGING_REPO}:base && \
        echo "Build docker image successful."
}

function show_helper() {
    echo "Syntax: $0 <environment>"
    echo "Parameters:"
    echo "    1. environment: default: \"production\", values: [\"staging\", \"production\", \"base-prd\", \"base-staging\"]"
    echo "Received invalid parameters. \"$0 $*\""
}

function main() {
    if [ "${2}" != "" ]
    then
        show_helper $*
        return 1
    fi

    set_version

    export RAILS_ENV=${1}
    case ${1} in
    "")
        export RAILS_ENV="production"
        production_build
        ;;
    "production")
        production_build_n_push
        ;;
    "staging")
        staging_build
        ;;
    "base-prd")
        export RAILS_ENV="production"
        build_base_production_image
        ;;
    "base-staging")
        export RAILS_ENV="staging"
        build_base_staging_image
        ;;
    *)
        show_helper $*
        return 1
    esac
}

main $@
