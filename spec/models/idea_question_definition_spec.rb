# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IdeaQuestionDefinition, type: :model do
  fixtures :oauth_applications

  let(:idea_definition) { create :idea_definition_with_questions }

  describe '.create' do
    it 'success' do
      expect(idea_definition.questions).to be_present
    end
  end
end
