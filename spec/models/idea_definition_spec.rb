# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IdeaDefinition, type: :model do
  fixtures :oauth_applications

  let(:idea_definition) { create :idea_definition }

  describe '.create' do
    it 'success' do
      expect(idea_definition.workspace).to eq(idea_definition.user.workspace)
    end
  end
end
