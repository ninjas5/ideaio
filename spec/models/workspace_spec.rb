# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Workspace, type: :model do
  let(:workspace) { create :workspace }

  describe '.create' do
    it 'success' do
      expect(workspace).not_to be_nil
    end
  end
end
