# frozen_string_literal: true

require 'rails_helper'

RSpec.describe QuestionAnswer, type: :model do
  fixtures :oauth_applications

  let(:idea_participant) { create :idea_participant_with_question_answers }

  describe '.create' do
    it 'success' do
      expect(idea_participant.question_answers).to be_present
    end
  end
end
