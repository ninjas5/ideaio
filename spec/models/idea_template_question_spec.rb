# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IdeaTemplateQuestion, type: :model do
  fixtures :oauth_applications

  let(:idea_template) { create :idea_template_with_questions }

  describe '.create' do
    it 'success' do
      expect(idea_template.questions).to be_present
    end
  end
end
