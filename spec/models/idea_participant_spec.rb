# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IdeaParticipant, type: :model do
  fixtures :oauth_applications

  let(:idea_participant) { create :idea_participant }

  describe '.create' do
    it 'success' do
      expect(idea_participant).not_to be_nil
    end
  end
end
