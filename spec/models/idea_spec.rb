# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Idea, type: :model do
  fixtures :oauth_applications

  let(:idea) { create :idea }

  describe '.create' do
    it 'success' do
      expect(idea).not_to be_nil
    end
  end
end
