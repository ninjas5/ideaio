# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IdeaQuestion, type: :model do
  fixtures :oauth_applications

  let(:idea) { create :idea_with_questions }

  describe '.create' do
    it 'success' do
      expect(idea.questions).to be_present
    end
  end
end
