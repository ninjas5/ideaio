# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AnswerComment, type: :model do
  fixtures :oauth_applications

  let(:answer_comment) { create :answer_comment }

  describe '.create' do
    it 'success' do
      expect(answer_comment).not_to be_nil
    end
  end
end
