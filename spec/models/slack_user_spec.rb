# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SlackUser, type: :model do
  fixtures :oauth_applications

  let(:user) { create :user }

  describe '.create' do
    it 'success' do
      expect(user.slack_user).not_to be_nil
    end
  end
end
