# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IdeaTemplate, type: :model do
  fixtures :oauth_applications

  let(:idea_template) { create :idea_template }

  describe '.create' do
    it 'success' do
      expect(idea_template.workspace).to eq(idea_template.user.workspace)
    end
  end
end
