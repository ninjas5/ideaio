# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'Home', type: :request do
  fixtures :oauth_applications

  path '/ping' do
    get 'Health check' do
      tags 'Home'
      consumes 'application/json'
      produces 'application/json'

      response 200, 'Success' do
        examples 'text/html' => 'Pong'

        schema type: :string

        run_test! do |response|
          expect(response.body).to eq('Pong')
        end
      end
    end
  end
end
