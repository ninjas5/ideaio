# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'api/v1/idea_definitions', type: :request do
  fixtures :oauth_applications

  EXAMPLE_IDEA_DEFINITIONS = [ # rubocop:disable RSpec/LeakyConstantDeclaration
    {
      id: 1,
      workspace_id: 11,
      user_id: 231,
      idea_template_id: 123,
      name: 'Daily standup',
      description: 'daily standup feedback',
      status: IdeaTemplate::Status::ENABLED,
      repeatable: true,
      timezone: CommonConst::ORIGIN_TIMEZONES.first,
      timer: {
        start_time: '2022-01-01 12:00:00',
        release_time: '2022-01-01 13:00:00',
      },
      channels: [
        { category: Workspace::Category::SLACK, ref_team_id: 'slack_team_1', channel_id: 'channel_1', name: 'general' },
      ],
      invited_participants: [
        { category: Workspace::Category::SLACK, ref_team_id: 'slack_team_1', ref_user_id: 'slack_user_1',
          email: 'user1@test.com', name: 'user1' },
        { category: Workspace::Category::SLACK, ref_team_id: 'slack_team_1', ref_user_id: 'slack_user_2',
          email: 'user2@test.com', name: 'user2' },
        { category: Workspace::Category::SLACK, ref_team_id: 'slack_team_1', ref_user_id: 'slack_user_3',
          email: 'user3@test.com', name: 'user3' },
      ],
      report_shareable: true,
      anonymous: false,
      archived_at: nil,
      questions: [
        { id: 1, ordinal: 0, required: true, content: '昨天做了什么？' },
        { id: 2, ordinal: 1, required: true, content: '今天计划做什么？' },
        { id: 3, ordinal: 2, required: true, content: '有什么被block么？' },
      ],
    },
    {
      id: 2,
      workspace_id: 11,
      user_id: 231,
      idea_template_id: 123,
      name: 'Daily standup',
      description: 'daily standup feedback',
      status: IdeaTemplate::Status::ENABLED,
      repeatable: true,
      timezone: CommonConst::ORIGIN_TIMEZONES.first,
      timer: {
        cycle_type: 'weekly',
        cycle_count: 1,
        start_date: '2022-04-11',
        end_date: nil,
        timer_triggers: [
          { ordinal: 0, start_time: '09:00', period_time: '30.minutes' },
          { ordinal: 1, start_time: '09:00:00', period_time: '30.minutes' },
          { ordinal: 2, start_time: '09:00:59', period_time: '30.minutes' },
          { ordinal: 3, start_time: '09:59', period_time: '30.minutes' },
          { ordinal: 4, start_time: '09:59:59', period_time: '30.minutes' },
        ],
      },
      channels: [
        { category: Workspace::Category::SLACK, ref_team_id: 'slack_team_1', channel_id: 'channel_1', name: 'general' },
      ],
      invited_participants: [
        { category: Workspace::Category::SLACK, ref_team_id: 'slack_team_1', ref_user_id: 'slack_user_4',
          email: 'user4@test.com', name: 'user4' },
        { category: Workspace::Category::SLACK, ref_team_id: 'slack_team_1', ref_user_id: 'slack_user_5',
          email: 'user5@test.com', name: 'user5' },
        { category: Workspace::Category::SLACK, ref_team_id: 'slack_team_1', ref_user_id: 'slack_user_6',
          email: 'user6@test.com', name: 'user6' },
      ],
      report_shareable: true,
      anonymous: false,
      archived_at: nil,
      questions: [
        { id: 4, ordinal: 0, required: true, content: '昨天做了什么？' },
        { id: 5, ordinal: 1, required: true, content: '今天计划做什么？' },
        { id: 6, ordinal: 2, required: true, content: '有什么被block么？' },
      ],
    },
  ].freeze

  path '/api/v1/idea_definitions' do
    post 'Create idea definition' do
      tags 'IdeaDefinition'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      parameter name: :request_body, in: :body, schema: { '$ref' => '#/components/schemas/idea_definition_input' }

      let(:workspace) { build :workspace }
      let(:request_body) { { idea_definition: idea_definition_hash } }
      let(:idea_definition_hash) do
        {
          idea_template_id: idea_template_id,
          name: 'Daily standup',
          description: 'daily standup feedback',
          timezone: timezone,
          status: IdeaDefinition::Status::ENABLED,
          repeatable: idea_repeatable,
          timer: timer,
          channels: invited_channels,
          invited_participants: invited_participants,
          anonymous: false,
          report_shareable: true,
          questions: [
            { id: 1, required: true, content: '昨天做了什么？' },
            { required: true, content: '今天计划做什么？' },
            { required: true, content: '有什么被block么？' },
          ],
        }
      end
      let(:invited_channels) { [] }
      let(:invited_participants) { [] }
      # Single timer
      let(:idea_repeatable) { false }
      let(:timer) do
        {
          start_time: 1.hour.after.in_time_zone(timezone).strftime('%Y-%m-%d %H:%M:%S'),
          release_time: 2.hours.after.in_time_zone(timezone).strftime('%Y-%m-%d %H:%M:%S'),
        }
      end
      let(:timezone) { CommonConst::ORIGIN_TIMEZONES.sample }
      let(:idea_template_id) { nil }

      context 'with valid params' do
        let(:user) { create :user, workspace: workspace }
        let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

        let(:slack_team) { workspace.slack_team }
        let(:invited_channels) do
          [
            { category: Workspace::Category::SLACK, ref_team_id: slack_team.ref_team_id, channel_id: 'channel_1',
              name: 'general' },
            { category: Workspace::Category::SLACK, ref_team_id: slack_team.ref_team_id, channel_id: 'channel_2',
              name: 'fun' },
          ]
        end
        let(:invited_participants) do
          [
            { category: Workspace::Category::SLACK, ref_team_id: slack_team.ref_team_id, ref_user_id: 'slack_user_1',
              email: 'user1@test.com', name: 'user1' },
            { category: Workspace::Category::SLACK, ref_team_id: slack_team.ref_team_id, ref_user_id: 'slack_user_2',
              email: 'user2@test.com', name: 'user2' },
            { category: Workspace::Category::SLACK, ref_team_id: slack_team.ref_team_id, ref_user_id: 'slack_user_3',
              email: 'user3@test.com', name: 'user3' },
          ]
        end

        let(:idea_template_id) { idea_template.id }
        let(:idea_template) { create :idea_template_with_questions, workspace: nil, user: nil }

        # TODO: 暂不支持
        # response 200, 'create idea definition with cycle timer' do
        #   examples 'application/json' => {
        #     code: 0,
        #     message: 'success',
        #     idea_definition: EXAMPLE_IDEA_DEFINITIONS.second,
        #   }

        #   schema type: :object,
        #     properties: {
        #       code: { type: :integer },
        #       message: { type: :string },
        #       idea_definition: { '$ref': '#/components/schemas/idea_definition' },
        #     },
        #     required: %w[code message idea_definition]

        #   # Cycle timer
        #   let(:idea_repeatable) { true }
        #   let(:timer) do
        #     {
        #       cycle_type: 'weekly',
        #       cycle_count: 1,
        #       start_date: '2022-04-11',
        #       end_date: nil,
        #       timer_triggers: [
        #         { ordinal: 0, start_time: '09:00', period_time: '30.minutes' },
        #         { ordinal: 1, start_time: '09:00:00', period_time: '30.minutes' },
        #         { ordinal: 2, start_time: '09:00:59', period_time: '30.minutes' },
        #         { ordinal: 3, start_time: '09:59', period_time: '30.minutes' },
        #         { ordinal: 4, start_time: '09:59:59', period_time: '30.minutes' },
        #       ],
        #     }
        #   end

        #   run_test! do |response|
        #     data = JSON.parse response.body
        #     expect(data['code']).to be_zero
        #     expect(data['idea_definition']['name']).to eq(idea_definition_hash[:name])
        #     expect(data['idea_definition']['questions'].size).to eq(idea_definition_hash[:questions].size)

        #     expect(data['idea_definition']['timer']['cycle_type']).to eq(timer[:cycle_type])
        #     expect(data['idea_definition']['timer']['cycle_count']).to eq(timer[:cycle_count])
        #     expect(data['idea_definition']['timer']['timer_triggers'].size).to eq(timer[:timer_triggers].size)
        #     data['idea_definition']['timer']['timer_triggers'].each_with_index do |trigger, index|
        #       expect(trigger.with_indifferent_access).to eq(timer[:timer_triggers][index].with_indifferent_access)
        #     end

        #     data['idea_definition']['questions'].each do |question|
        #       expect(question['content']).to eq(idea_definition_hash[:questions][question['ordinal']][:content])
        #     end
        #   end
        # end

        response 200, 'Create idea definition with simple timer' do
          examples 'application/json' => {
            code: 0,
            message: 'success',
            idea_definition: EXAMPLE_IDEA_DEFINITIONS.first,
          }

          schema type: :object,
            properties: {
              code: { type: :integer },
              message: { type: :string },
              idea_definition: { '$ref': '#/components/schemas/idea_definition' },
            },
            required: %w[code message]

          run_test! do |response|
            data = JSON.parse response.body
            expect(data['code']).to be_zero
            expect(data['idea_definition']['name']).to eq(idea_definition_hash[:name])
            expect(data['idea_definition']['questions'].size).to eq(idea_definition_hash[:questions].size)

            expect(data['idea_definition']['timer']['start_time']).to eq(timer[:start_time])
            expect(data['idea_definition']['timer']['release_time']).to eq(timer[:release_time])

            data['idea_definition']['questions'].each do |question|
              expect(question['content']).to eq(idea_definition_hash[:questions][question['ordinal']][:content])
            end
          end
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(CommonConst::ACCESS_KEY_NAME.to_sym) { 'Bearer token="invalid_token_tsetadfdfads"' }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end

    get 'List idea definitions' do
      tags 'IdeaDefinition'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      # TODO: 等有需要再加
      # parameter name: :running_status,
      #   in: :query,
      #   schema: { type: :string, enum: Idea::RunningStatus.const_values },
      #   required: false,
      #   description: 'Idea definition running status'

      response 200, 'Search idea definitions with simple timer' do
        examples 'application/json' => {
          code: 0,
          message: 'success',
          idea_definitions: [
            {
              id: 1,
              user_id: 1,
              name: 'Daily standup',
              running_status: Idea::RunningStatus::STARTED,
              start_time: '2022-01-01T09:00:00z',
              release_time: '2022-01-01T09:30:00z',
              idea_id: nil,
            },
            {
              id: 2,
              user_id: 1,
              name: 'Daily standup 2',
              running_status: Idea::RunningStatus::STARTED,
              start_time: '2022-01-05T09:00:00z',
              release_time: '2022-01-05T09:30:00z',
              idea_id: 2,
            },
          ],
          users: [
            {
              id: 1,
              slack_user: {
                ref_team_id: 'slack_team_1',
                ref_user_id: 'slack_user_1',
                email: 'cris@test.com',
                profile: {
                  avatar_hash: 'cris@test.com',
                  email: 'cris@test.com',
                  real_name: 'cris@test.com',
                  team: 'cris@test.com',
                },
              },
            },
          ],
        }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
            idea_definitions: {
              type: :array,
              items: { '$ref': '#/components/schemas/idea_definition_abbr' },
            },
            users: {
              type: :array,
              items: { '$ref': '#/components/schemas/user' },
            },
          },
          required: %w[code message idea_definitions users]

        let(:workspace) { build :workspace }
        let(:user) { create :user, workspace: workspace }
        let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

        let!(:idea) do
          create :idea_with_questions,
            workspace: workspace,
            idea_definition: idea_definitions.first,
            running_status: Idea::RunningStatus::STARTED
        end
        let(:idea_definitions) do
          create_list :idea_definition_with_questions, idea_size, workspace: workspace, user: user
        end
        let(:idea_size) { 3 }
        let!(:other_idea_definitions) do # rubocop:disable RSpec/LetSetup
          create_list :idea_definition_with_questions, other_idea_size, workspace: workspace
        end
        let(:other_idea_size) { 3 }

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
          expect(data['idea_definitions'].size).to eq(idea_size)
          expect(data['users']).to be_present

          data['idea_definitions'].each do |definition|
            if definition['id'].eql?(idea.idea_definition_id)
              expect(definition['running_status']).eql?(idea.running_status)
            else
              expect(definition['running_status']).eql?(Idea::RunningStatus::STARTED)
            end
          end
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(CommonConst::ACCESS_KEY_NAME.to_sym) { 'Bearer token="invalid_token_tsetadfdfads"' }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end
  end

  path '/api/v1/idea_definitions/{id}' do
    parameter name: :id,
      in: :path,
      type: :integer,
      required: true,
      description: 'Idea definition unique id'

    get 'Show idea definition' do
      tags 'IdeaDefinition'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      response 200, 'Success' do
        examples 'application/json' => {
          code: 0,
          message: 'success',
          idea_definitions: EXAMPLE_IDEA_DEFINITIONS.first,
        }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
            idea_definition: { '$ref': '#/components/schemas/idea_definition' },
          },
          required: %w[code message]

        let(:workspace) { build :workspace }
        let(:user) { create :user, workspace: workspace }
        let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

        let(:idea_definition) do
          create :idea_definition_with_questions, workspace: workspace, user: user, timezone: timezone, timer: timer
        end

        let(:timer) do
          {
            start_time: 1.hour.after.in_time_zone(timezone).strftime('%Y-%m-%d %H:%M:%S'),
            release_time: 2.hours.after.in_time_zone(timezone).strftime('%Y-%m-%d %H:%M:%S'),
          }
        end
        let(:timezone) { CommonConst::ORIGIN_TIMEZONES.sample }
        let(:id) { idea_definition.id }

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(CommonConst::ACCESS_KEY_NAME.to_sym) { 'Bearer token="invalid_token_tsetadfdfads"' }
        let(:id) { 0 }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end

    put 'Update idea definition' do
      tags 'IdeaDefinition'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      parameter name: :request_body, in: :body, schema: { '$ref' => '#/components/schemas/idea_definition_input' }

      let(:workspace) { build :workspace }
      let(:request_body) { { idea_definition: idea_definition_hash } }
      let(:idea_definition_hash) do
        {
          idea_template_id: idea_template_id,
          name: 'Daily standup',
          description: 'daily standup feedback',
          timezone: timezone,
          repeatable: true,
          timer: timer,
          channels: invited_channels,
          invited_participants: invited_participants,
          report_shareable: true,
          questions: [
            { content: '昨天做了什么？' },
            { content: '今天计划做什么？' },
            { content: '有什么被block么？' },
          ],
        }
      end
      let(:invited_channels) { [] }
      let(:invited_participants) { [] }
      # Single timer
      let(:timer) do
        {
          start_time: 1.hour.after.in_time_zone(timezone).strftime('%Y-%m-%d %H:%M:%S'),
          release_time: 2.hours.after.in_time_zone(timezone).strftime('%Y-%m-%d %H:%M:%S'),
        }
      end
      let(:timezone) { CommonConst::ORIGIN_TIMEZONES.sample }
      let(:idea_template_id) { nil }

      response 200, 'Success' do
        examples 'application/json' => {
          code: 0,
          message: 'success',
        }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
          },
          required: %w[code message]

        let(:user) { create :user, workspace: workspace }
        let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

        let(:id) { idea_definition.id }
        let(:idea_definition) do
          create :idea_definition_with_questions, workspace: workspace, user: user, timezone: timezone, timer: timer
        end

        let(:timer) do
          {
            start_time: 1.hour.after.in_time_zone(timezone).strftime('%Y-%m-%d %H:%M:%S'),
            release_time: 2.hours.after.in_time_zone(timezone).strftime('%Y-%m-%d %H:%M:%S'),
          }
        end

        let(:slack_team) { workspace.slack_team }
        let(:invited_channels) do
          [
            { category: Workspace::Category::SLACK, ref_team_id: slack_team.ref_team_id, channel_id: 'channel_1',
              name: 'general' },
            { category: Workspace::Category::SLACK, ref_team_id: slack_team.ref_team_id, channel_id: 'channel_2',
              name: 'fun' },
          ]
        end
        let(:invited_participants) do
          [
            { category: Workspace::Category::SLACK, ref_team_id: slack_team.ref_team_id, ref_user_id: 'slack_user_1',
              email: 'user1@test.com', name: 'user1' },
            { category: Workspace::Category::SLACK, ref_team_id: slack_team.ref_team_id, ref_user_id: 'slack_user_2',
              email: 'user2@test.com', name: 'user2' },
            { category: Workspace::Category::SLACK, ref_team_id: slack_team.ref_team_id, ref_user_id: 'slack_user_3',
              email: 'user3@test.com', name: 'user3' },
          ]
        end

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(CommonConst::ACCESS_KEY_NAME.to_sym) { 'Bearer token="invalid_token_tsetadfdfads"' }
        let(:id) { 0 }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end

    delete 'Delete idea definiton' do
      tags 'IdeaDefinition'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      response 200, 'Success' do
        examples 'application/json' => { code: 0, message: 'success' }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
          },
          required: %w[code message]

        let(:workspace) { build :workspace }
        let(:user) { create :user, workspace: workspace }
        let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

        let(:idea_definition) { create :idea_definition_with_questions, workspace: workspace, user: user }
        let(:id) { idea_definition.id }

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(CommonConst::ACCESS_KEY_NAME.to_sym) { 'Bearer token="invalid_token_tsetadfdfads"' }
        let(:id) { 0 }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end
  end
end
