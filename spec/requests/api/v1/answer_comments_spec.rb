# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'api/v1/answer_comments', type: :request do
  fixtures :oauth_applications

  path '/api/v1/question_answers/{question_answer_id}/comments' do
    parameter name: :question_answer_id,
      in: :path,
      type: :string,
      required: true,
      description: 'question_answer_id'

    parameter name: :request_body, in: :body, schema: {
      type: :object,
      properties: {
        content: { type: :string },
      },
      required: [:content],
    }
    let(:request_body) { { content: content } }
    let(:content) { 'just a answer comment' }
    let(:question_answer_id) { 0 }

    post 'create answer_comment' do
      tags 'AnswerComment'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      response 200, 'Success' do
        examples 'application/json' => { code: 0, message: 'success', id: 1 }

        schema type: :object,
          properties: { code: { type: :integer }, message: { type: :string }, id: { type: :integer } },
          required: %w[code message]

        let(:workspace) { build :workspace }
        let(:user) { create :user, workspace: workspace }
        let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

        let(:idea_participant) { create :idea_participant_with_question_answers, workspace: workspace, user: user }
        let(:question_answer) { idea_participant.question_answers.first }
        let(:question_answer_id) { question_answer.id }

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
          expect(data['id']).to eq(question_answer.comments.last.id)
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(CommonConst::ACCESS_KEY_NAME.to_sym) { 'Bearer token="invalid_token_tsetadfdfads"' }
        let(:id) { 0 }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end
  end

  path '/api/v1/question_answers/{question_answer_id}/comments/{id}' do
    parameter name: :question_answer_id,
      in: :path,
      type: :string,
      required: true,
      description: 'question_answer_id'

    parameter name: :id,
      in: :path,
      type: :string,
      required: true,
      description: 'answer comment id'

    let(:question_answer_id) { 0 }
    let(:id) { 0 }

    delete 'delete answer_comment' do
      tags 'AnswerComment'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      response 200, 'Success' do
        examples 'application/json' => { code: 0, message: 'success' }

        schema type: :object,
          properties: { code: { type: :integer }, message: { type: :string } },
          required: %w[code message]

        let(:workspace) { build :workspace }
        let(:user) { create :user, workspace: workspace }
        let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

        let(:idea_participant) { create :idea_participant_with_question_answers, workspace: workspace, user: user }
        let(:question_answer) { idea_participant.question_answers.first }
        let(:question_answer_id) { question_answer.id }
        let(:comment) { create :answer_comment, workspace: workspace, question_answer: question_answer, user: user }
        let(:id) { comment.id }

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(CommonConst::ACCESS_KEY_NAME.to_sym) { 'Bearer token="invalid_token_tsetadfdfads"' }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end
  end
end
