# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'api/v1/slack', type: :request do
  fixtures :oauth_applications

  path '/api/v1/logout' do
    post('logout slack') do
      tags 'Slack'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      response 200, 'Success' do
        examples 'application/json' => { code: 0, message: 'success' }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
          },
          required: %w[code message]

        let(:user) { create :user }
        let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

        # rubocop:disable RSpec/ExpectInHook
        # rubocop:disable RSpec/ScatteredSetup
        before do
          expect(::UserService).to receive(:logout).once.with(user)
        end
        # rubocop:enable RSpec/ExpectInHook
        # rubocop:enable RSpec/ScatteredSetup

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(CommonConst::ACCESS_KEY_NAME.to_sym) { 'Bearer token="invalid_token_tsetadfdfads"' }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end
  end

  path '/api/v1/slack/oauth_authorize' do
    get('oauth_authorize slack') do
      tags 'Slack'
      consumes 'application/json'
      produces 'application/json'

      parameter name: :code, in: :query, required: true, type: :string
      parameter name: :state, in: :query, required: true, type: :string
      parameter name: :error, in: :query, required: false, type: :string

      response(200, 'successful') do
        examples 'application/json' => { code: 0, message: 'success', access_token: 'access_token' }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
            token: { type: :string },
            expires_at: { type: [:string, nil] },
          },
          required: %w[code message]

        let(:user) { create :user }
        let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }
        let(:code) { '123123' }
        let(:state) { '' }

        # rubocop:disable RSpec/ExpectInHook
        # rubocop:disable RSpec/ScatteredSetup
        before do
          expect(::SlackService).to receive(:oauth_access).once.with(code).and_return(user.access_tokens.last)
        end
        # rubocop:enable RSpec/ExpectInHook
        # rubocop:enable RSpec/ScatteredSetup

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
          expect(data['token']).to eq(user.access_tokens.last.token)
        end
      end
    end
  end
end
