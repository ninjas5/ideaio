# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'api/v1/users', type: :request do
  fixtures :oauth_applications

  path '/api/v1/profile' do
    get("Show current user's profile") do
      tags 'User'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      response 200, 'Success' do
        examples 'application/json' => {
          code: 0,
          message: 'success',
          user: {
            id: 1,
            slack_user: {
              ref_team_id: 'slack_team_id_1',
              ref_user_id: 'ref_user_id_1',
              email: 'email_1@slack_user.com',
              profile: {
                avatar_hash: 'email_1@slack_user.com',
                email: 'email_1@slack_user.com',
                real_name: 'email_1@slack_user.com',
              },
            },
          },
        }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
            user: { '$ref': '#/components/schemas/user' },
          },
          required: %w[code message user]

        let(:user) { create :user }
        let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
          expect(data['user']['id']).to eq(user.id)
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(CommonConst::ACCESS_KEY_NAME.to_sym) { 'Bearer token="invalid_token_tsetadfdfads"' }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end
  end

  path '/api/v1/users/archive' do
    post('Archive user account') do
      tags 'User'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      let(:user) { create :user, archived_at: archived_at }
      let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

      response 200, 'Success' do
        examples 'application/json' => { code: 0, message: 'success' }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
          },
          required: %w[code message]

        let(:archived_at) { nil }

        # rubocop:disable RSpec/ExpectInHook
        before do
          expect(DeleteArchivedUserJob).to receive(:set).once.and_return(DeleteArchivedUserJob)
          expect(DeleteArchivedUserJob).to receive(:perform_later).once.with(user.id)
        end
        # rubocop:enable RSpec/ExpectInHook

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero

          latest_user = User.find(user.id)
          expect(latest_user).to be_archived
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(:archived_at) { 1.hour.ago }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end
  end

  path '/api/v1/users/access_tokens' do
    get('List all user and access_tokens') do
      tags 'User'
      consumes 'application/json'
      produces 'application/json'

      response 200, 'Success' do
        examples 'application/json' => {
          code: 0,
          message: 'success',
          users: [
            {
              id: 1,
              email: 'user1@test.com',
              active_access_tokens: [
                {
                  id: 1,
                  token: 'token1',
                  expires_at: Time.zone.parse('2022-01-01 12:23:34'),
                  revoked_at: nil,
                  created_at: Time.zone.parse('2022-01-01 02:23:34'),
                },
              ],
            },
          ],
        }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
            users: {
              type: :array,
              items: {
                type: :object,
                properties: {
                  id: { type: :integer },
                  email: { type: :string },
                  active_access_tokens: {
                    type: :array,
                    items: {
                      type: :object,
                      properties: {
                        id: { type: :integer },
                        token: { type: :string },
                        expires_in: { type: [:integer, nil] },
                        expires_at: { type: [:string, nil] },
                        revoked_at: { type: [:string, nil] },
                        created_at: { type: :string },
                      },
                      required: %i[id token created_at],
                    },
                  },
                },
                required: %i[id email active_access_tokens],
              },
            },
          },
          required: %w[code message users]

        let!(:users) { create_list :user, 3 } # rubocop:disable RSpec/LetSetup

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
          expect(data['users']).to be_present
        end
      end
    end
  end
end
