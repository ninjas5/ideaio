# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'Workspaces API', type: :request do
  fixtures :oauth_applications

  path '/api/v1/workspaces' do
    get 'Get all workspaces' do
      tags 'Workspace'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      response 200, 'Success' do
        examples 'application/json' => {
          code: 0,
          message: 'success',
          workspaces: [
            {
              id: 1,
              category: 'slack',
              name: 'skytest',
              archived_at: nil,
              created_at: '2022-04-06T15:59:38.706Z',
              updated_at: '2022-04-06T15:59:38.706Z',
            },
          ],
        }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
            workspaces: {
              type: :array,
              items: {
                '$ref': '#/components/schemas/workspace',
              },
            },
          },
          required: %w[code message workspaces]

        let(:user) { create :user }
        let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
          expect(data['workspaces'].size).to eq(1)
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(CommonConst::ACCESS_KEY_NAME.to_sym) { 'Bearer token="invalid_token_tsetadfdfads"' }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end
  end
end
