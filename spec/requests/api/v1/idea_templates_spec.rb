# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'api/v1/idea_templates', type: :request do
  fixtures :oauth_applications

  let(:workspace) { create :workspace }
  let(:user) { create :user, workspace: workspace }
  let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

  let!(:global_template) { create :idea_template_with_questions, workspace: nil, user: nil }
  let!(:workspace_template) { create :idea_template_with_questions, workspace: workspace, user: nil }
  let!(:user_template) { create :idea_template_with_questions, workspace: workspace, user: user }
  let!(:other_template) { create :idea_template } # rubocop:disable RSpec/LetSetup

  path '/api/v1/idea_templates' do
    get 'Get all templates' do
      tags 'IdeaTemplate'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      response 200, 'Success' do
        examples 'application/json' => {
          code: 0,
          message: 'success',
          idea_templates: [
            {
              id: 1,
              category: IdeaTemplate::Category::GENERAL,
              name: 'Daily standup',
              description: 'daily standup feedback',
            },
          ],
        }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
            idea_templates: {
              type: :array,
              items: {
                '$ref': '#/components/schemas/idea_template',
              },
            },
          },
          required: %w[code message idea_templates]

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
          expect(data['idea_templates'].size).to eq(3)

          expected_template_ids = [global_template.id, workspace_template.id, user_template.id]
          data['idea_templates'].each do |template|
            expect(expected_template_ids).to include(template['id'])
          end
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(CommonConst::ACCESS_KEY_NAME.to_sym) { 'Bearer token="invalid_token_tsetadfdfads"' }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end
  end

  path '/api/v1/idea_templates/{id}' do
    get 'Get questions by template id' do
      tags 'IdeaTemplate'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      parameter name: :id,
        in: :path,
        type: :integer,
        required: true,
        description: 'Template id'

      let(:id) { global_template.id }

      response 200, 'Success' do
        examples 'application/json' => {
          code: 0,
          message: 'success',
          name: 'Template name',
          description: 'Template description',
          questions: [
            {
              id: 1,
              idea_template_id: 3,
              ordinal: 0,
              content: 'Do you think we achieved our last quarter goals? Why and why not?',
            },
            {
              id: 2,
              idea_template_id: 3,
              ordinal: 1,
              content: 'Any relevant data or metrics you want to share or review?',
            },
            {
              id: 3,
              idea_template_id: 4,
              ordinal: 1,
              content: 'What challenges do you meet in the past quarter?',
            },
          ],
        }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
            name: { type: :string },
            description: { type: :string },
            questions: {
              type: :array,
              items: {
                '$ref': '#/components/schemas/question',
              },
            },
          },
          required: %w[code message]

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
          expect(data['questions']).to be_present

          expected_question_ids = global_template.questions.ids
          data['questions'].each do |question|
            expect(expected_question_ids).to include(question['id'])
          end
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(CommonConst::ACCESS_KEY_NAME.to_sym) { 'Bearer token="invalid_token_tsetadfdfads"' }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end
  end
end
