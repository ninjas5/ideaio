# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'api/v1/settings', type: :request do
  path '/api/v1/settings/timezones' do
    get('List timezones') do
      tags 'Settings'
      consumes 'application/json'
      produces 'application/json'

      response(200, 'successful') do
        examples 'application/json' => {
          code: 0,
          message: 'success',
          timezones: [
            { timezone: 'Asia/Shanghai', display_name: '(GMT+08:00) Asia/Shanghai' },
            { timezone: 'Asia/Hong_Kong', display_name: '(GMT+08:00) Asia/Hong_Kong' },
          ],
        }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
            timezones: {
              type: :array,
              items: {
                type: :object,
                properties: {
                  timezone: { type: :string, enum: CommonConst::ORIGIN_TIMEZONES },
                  display_name: { type: :string },
                },
                required: %i[timezone display_name],
              },
            },
          },
          required: %w[code message timezones]

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
          expect(data['timezones']).to be_present
        end
      end
    end
  end
end
