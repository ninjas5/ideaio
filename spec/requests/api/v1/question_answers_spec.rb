# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'api/v1/question_answers', type: :request do
  fixtures :oauth_applications

  path '/api/v1/question_answers/{id}/vote' do
    parameter name: :id, in: :path, type: :integer, description: 'id'
    let(:id) { 0 }

    post 'vote question_answer' do
      tags 'Idea'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      response 200, 'Success' do
        examples 'application/json' => { code: 0, message: 'success', question_answer: { id: 1, vote_user_ids: [1] } }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
            question_answer: {
              type: :object,
              properties: {
                id: { type: :integer },
                vote_user_ids: { type: :array, item: { types: :integer } },
              },
              required: %i[id vote_user_ids],
            },
          },
          required: %w[code message]

        let(:workspace) { build :workspace }
        let(:user) { create :user, workspace: workspace }
        let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

        let(:submit_user) { create :user, workspace: workspace }
        let(:idea_participant) do
          create :idea_participant_with_question_answers, workspace: workspace, user: submit_user
        end
        let(:question_answer) { idea_participant.question_answers.first }
        let(:id) { question_answer.id }

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
          expect(data['question_answer']['vote_user_ids']).to include(user.id)
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(CommonConst::ACCESS_KEY_NAME.to_sym) { 'Bearer token="invalid_token_tsetadfdfads"' }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end
  end
end
