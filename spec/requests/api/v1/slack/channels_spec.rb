# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'api/v1/slack/channels', type: :request do
  fixtures :oauth_applications

  path '/api/v1/slack/channels' do
    get 'List slack channels' do
      tags 'Slack'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      response 200, 'Success' do
        examples 'application/json' => {
          code: 0,
          message: 'success',
          channels: [
            {
              id: 'C024BE91K',
              name: 'more fun',
              team_id: 'T024BE912',
            },
            {
              id: 'C024BE91L',
              name: 'fun',
              team_id: 'T024BE911',
              num_members: 34,
            },
            {
              id: 'C024BE91M',
              name: 'public-channel',
              team_id: 'T024BE911',
              is_redacted: true,
              num_members: 34,
            },
          ],
        }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
            channels: {
              type: :array,
              items: {
                type: :object,
                properties: {
                  id: { type: :string },
                  name: { type: :string },
                  team_id: { type: :string },
                  is_redacted: { type: :boolean },
                  num_members: { type: :integer },
                },
                required: %i[id name team_id],
              },
            },
          },
          required: %w[code message]

        let(:user) { create :user }
        let(:slack_user) { user.slack_user }
        let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

        before do
          allow(SlackService).to receive(:list_channels).with(user.workspace).and_return(Hashie::Mash.new(
            ok: true,
            channels: [
              { id: 'C024BE91K', name: 'more fun', team_id: 'T024BE912' },
              { id: 'C024BE91L', name: 'fun', team_id: 'T024BE911', num_members: 34 },
              { id: 'C024BE91M', name: 'public-channel', team_id: 'T024BE911', is_redacted: true, num_members: 34 },
            ],
          ))
        end

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
          expect(data['channels']).to be_present
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(CommonConst::ACCESS_KEY_NAME.to_sym) { 'Bearer token="invalid_token_tsetadfdfads"' }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end
  end
end
