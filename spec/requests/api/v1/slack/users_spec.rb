# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'api/v1/slack/users', type: :request do
  fixtures :oauth_applications

  path '/api/v1/slack/users' do
    get 'List of slack users' do
      tags 'Slack'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      parameter name: :channels,
        in: :query,
        type: :string,
        required: false,
        description: 'Channel Ids, example: "channel_id1,channel_id2"'

      response 200, 'Success' do
        examples 'application/json' => {
          code: 0,
          message: 'success',
          users: Api::V1::Slack::UsersController::MOCKED_USERS,
        }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
            users: {
              type: :array,
              items: { '$ref' => '#/components/schemas/slack_user' },
            },
          },
          required: %w[code message]

        let(:user) { create :user }
        let(:slack_user) { user.slack_user }
        let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

        let(:search_text) { 'test' }
        let(:channel_ids) { %w[channel_id_1 channel_id_2] }
        let(:channels) { channel_ids.join(',') }

        before do
          allow(SlackService).to receive(:list_users).with(user.workspace, channel_ids).and_return([
            {
              ref_user_id: 'W012A3CDE',
              ref_team_id: 'T012AB3C4',
              email: 'spengler@ghostbusters.example.com',
              profile: {
                avatar_hash: 'ge3b51ca72de',
                status_text: 'Print is dead',
                status_emoji: ':books:',
                real_name: 'Egon Spengler',
                display_name: 'spengler',
                real_name_normalized: 'Egon Spengler',
                display_name_normalized: 'spengler',
                email: 'spengler@ghostbusters.example.com',
                image_24: 'https://.../avatar/e3b51ca72dee4ef87916ae2b9240df50.jpg',
                image_32: 'https://.../avatar/e3b51ca72dee4ef87916ae2b9240df50.jpg',
                image_48: 'https://.../avatar/e3b51ca72dee4ef87916ae2b9240df50.jpg',
                image_72: 'https://.../avatar/e3b51ca72dee4ef87916ae2b9240df50.jpg',
                image_192: 'https://.../avatar/e3b51ca72dee4ef87916ae2b9240df50.jpg',
                image_512: 'https://.../avatar/e3b51ca72dee4ef87916ae2b9240df50.jpg',
                team: 'T012AB3C4',
              },
            },
            {
              ref_user_id: 'W07QCRPA4',
              ref_team_id: 'T0G9PQBBK',
              email: 'glenda@south.oz.coven',
              profile: {
                avatar_hash: '8fbdd10b41c6',
                image_24: 'https://a.slack-edge.com...png',
                image_32: 'https://a.slack-edge.com...png',
                image_48: 'https://a.slack-edge.com...png',
                image_72: 'https://a.slack-edge.com...png',
                image_192: 'https://a.slack-edge.com...png',
                image_512: 'https://a.slack-edge.com...png',
                image_1024: 'https://a.slack-edge.com...png',
                image_original: 'https://a.slack-edge.com...png',
                first_name: 'Glinda',
                last_name: 'Southgood',
                title: 'Glinda the Good',
                phone: '',
                skype: '',
                real_name: 'Glinda Southgood',
                real_name_normalized: 'Glinda Southgood',
                display_name: 'Glinda the Fairly Good',
                display_name_normalized: 'Glinda the Fairly Good',
                email: 'glenda@south.oz.coven',
              },
            },
          ])
        end

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
          expect(data['users']).to be_present
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(CommonConst::ACCESS_KEY_NAME.to_sym) { 'Bearer token="invalid_token_tsetadfdfads"' }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end
  end
end
