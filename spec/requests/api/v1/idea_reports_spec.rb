# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'api/v1/idea_reports', type: :request do
  fixtures :oauth_applications

  MOCKED_DATA = { # rubocop:disable RSpec/LeakyConstantDeclaration
    idea_reports: [
      {
        id: 1,
        name: 'Daily standup',
        description: 'Daily standup',
        start_time: '2022-01-01 09:00:00',
        release_time: '2022-01-01 09:30:00',
        questions: [
          {
            id: 1,
            ordinal: 0,
            content: '昨天做了什么？',
            answers: [
              {
                id: 1,
                content: '打了两瓶酱油',
                tags: %w[fun lazy],
                vote_user_ids: [2, 3],
                created_at: '2022-01-01T12:12:12z',
                updated_at: '2022-01-01T12:12:12z',
                user_id: 1,
                comments: [
                  {
                    id: 1,
                    content: '这么闲鱼呀',
                    user_id: 2,
                    created_at: '2022-03-01T12:00:00z',
                  },
                  {
                    id: 2,
                    content: '是呀 是呀',
                    user_id: 1,
                    created_at: '2022-03-01T12:00:00z',
                  },
                  {
                    id: 3,
                    content: '有工作 好羡慕呀',
                    user_id: 3,
                    created_at: '2022-03-01T12:00:00z',
                  },
                ],
              },
              {
                id: 2,
                content: '送了一天外卖',
                tags: %w[work busy],
                vote_user_ids: [1, 3],
                created_at: '2022-01-01T12:12:12z',
                updated_at: '2022-01-01T12:22:12z',
                user_id: 2,
                comments: [
                  {
                    id: 21,
                    content: '有工作真好 求介绍',
                    user_id: 3,
                    created_at: '2022-03-01T12:00:00z',
                  },
                  {
                    id: 22,
                    content: '好累的',
                    user_id: 2,
                    created_at: '2022-03-01T12:00:01z',
                  },
                  {
                    id: 23,
                    content: '干嘛这么累 躺家里不香吗',
                    user_id: 1,
                    created_at: '2022-03-01T12:00:02z',
                  },
                ],
              },
              {
                id: 3,
                content: '投简历 找工作',
                tags: ['work'],
                vote_user_ids: [2],
                created_at: '2022-01-01T12:12:12z',
                updated_at: '2022-01-01T12:22:12z',
                user_id: 3,
                comments: [
                  {
                    id: 31,
                    content: '来 送外卖 我带你',
                    user_id: 2,
                    created_at: '2022-03-01T12:00:00z',
                  },
                  {
                    id: 32,
                    content: '一起躺平',
                    user_id: 1,
                    created_at: '2022-03-01T12:00:01z',
                  },
                ],
              },
            ],
          },
          {
            id: 2,
            ordinal: 1,
            content: '今天做什么？',
            answers: [
              {
                id: 21,
                content: '还有5栋楼的房租没收，今天收房租去',
                tags: %w[work busy],
                vote_user_ids: [2, 3],
                created_at: '2022-01-01T12:12:12z',
                updated_at: '2022-01-01T12:12:12z',
                user_id: 1,
                comments: [
                  {
                    id: 211,
                    content: '土豪哥 咱们这么熟 又便宜的房子 出租不',
                    user_id: 2,
                    created_at: '2022-03-01T12:00:00z',
                  },
                  {
                    id: 222,
                    content: '大佬 缺腿毛挂件me 带带我',
                    user_id: 3,
                    created_at: '2022-03-01T12:00:00z',
                  },
                  {
                    id: 231,
                    content: '不敢不敢 我还羡慕你们这么积极向上',
                    user_id: 3,
                    created_at: '2022-03-01T12:00:00z',
                  },
                ],
              },
              {
                id: 22,
                content: '上午送外卖，下午请假去换个电瓶',
                tags: %w[work busy],
                vote_user_ids: [1, 3],
                created_at: '2022-01-01T12:12:12z',
                updated_at: '2022-01-01T12:22:12z',
                user_id: 2,
                comments: [
                  {
                    id: 221,
                    content: '哥 你换电瓶几多钱呀 我还有几个 并且我也会修',
                    user_id: 3,
                    created_at: '2022-03-01T12:00:00z',
                  },
                  {
                    id: 222,
                    content: '50块钱 你能便宜的话 我可以来找你',
                    user_id: 2,
                    created_at: '2022-03-01T12:00:01z',
                  },
                  {
                    id: 223,
                    content: '40块钱 咋样 我在南山地铁站旁边',
                    user_id: 3,
                    created_at: '2022-03-01T12:00:02z',
                  },
                ],
              },
              {
                id: 23,
                content: '投简历 找工作',
                tags: ['work'],
                vote_user_ids: [2],
                created_at: '2022-01-01T12:12:12z',
                updated_at: '2022-01-01T12:22:12z',
                user_id: 3,
                comments: [
                  {
                    id: 31,
                    content: '来 送外卖 我带你',
                    user_id: 2,
                    created_at: '2022-03-01T12:00:00z',
                  },
                  {
                    id: 32,
                    content: '一起躺平',
                    user_id: 1,
                    created_at: '2022-03-01T12:00:01z',
                  },
                ],
              },
            ],
          },
          {
            id: 3,
            ordinal: 3,
            content: '有啥新鲜事么？',
            answers: [
              {
                id: 31,
                content: '昨天成功约了一个漂亮小姐姐',
                tags: ['fun'],
                vote_user_ids: [2, 3],
                created_at: '2022-01-01T12:12:12z',
                updated_at: '2022-01-01T12:12:12z',
                user_id: 1,
              },
              {
                id: 33,
                content: '坏事一堆 不想说',
                tags: ['fun'],
                vote_user_ids: [2],
                created_at: '2022-01-01T12:12:12z',
                updated_at: '2022-01-01T12:22:12z',
                user_id: 3,
              },
            ],
          },
        ],
      },
    ],
    users: [
      {
        id: 1,
        slack_user: {
          ref_team_id: 'slack_team_1',
          ref_user_id: 'slack_user_1',
          email: 'cris@test.com',
          profile: {
            avatar_hash: 'cris@test.com',
            email: 'cris@test.com',
            real_name: 'cris@test.com',
            team: 'cris@test.com',
          },
        },
      },
      {
        id: 2,
        slack_user: {
          ref_team_id: 'slack_team_1',
          ref_user_id: 'slack_user_2',
          email: 'emma@test.com',
          profile: {
            avatar_hash: 'safds',
            email: 'emma@test.com',
            team: 'emma@test.com',
            real_name: 'emma@test.com',
          },
        },
      },
      {
        id: 3,
        slack_user: {
          ref_team_id: 'slack_team_1',
          ref_user_id: 'slack_user_3',
          email: 'alex@test.com',
          profile: {
            avatar_hash: 'alex@test.com',
            email: 'alex@test.com',
            real_name: 'alex@test.com',
            team: 'alex@test.com',
          },
        },
      },
    ],
  }.freeze

  path '/api/v1/idea_reports' do
    get 'List idea reports' do
      tags 'IdeaReport'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      response 200, 'Success' do
        examples 'application/json' => {
          code: 0,
          message: 'success',
          idea_reports: MOCKED_DATA[:idea_reports],
          users: MOCKED_DATA[:users],
        }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
            idea_reports: {
              type: :array,
              items: { '$ref': '#/components/schemas/idea_report_abbr' },
            },
            users: {
              type: :array,
              items: { '$ref': '#/components/schemas/user' },
            },
          },
          required: %w[code message idea_reports users]

        let(:workspace) { create :workspace }
        let(:user) { create :user, workspace: workspace }
        let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

        let(:created_idea_definition) { create :idea_definition_with_questions, workspace: workspace, user: user }
        let(:created_idea) do
          create :idea_with_questions, workspace: workspace, idea_definition: created_idea_definition,
            running_status: running_status
        end
        let!(:created_idea_participants) do # rubocop:disable RSpec/LetSetup
          create_list :idea_participant_with_question_answers, 3, workspace: workspace, idea: created_idea
        end

        let(:joined_idea_definition) { create :idea_definition_with_questions, workspace: workspace }
        let(:joined_idea) do
          create :idea_with_questions, workspace: workspace, idea_definition: joined_idea_definition,
            running_status: running_status, invited_participants: invited_participants, report_shareable: true
        end
        let(:invited_participants) do
          [{ category: Workspace::Category::SLACK, ref_team_id: slack_team.ref_team_id,
             ref_user_id: slack_user.ref_user_id, email: slack_user.email, name: slack_user.profile['real_name'] }]
        end
        let(:slack_user) { user.slack_user }
        let(:slack_team) { slack_user.slack_team }
        let!(:joined_idea_participant) do # rubocop:disable RSpec/LetSetup
          create :idea_participant_with_question_answers, workspace: workspace, idea: joined_idea, user: user
        end

        let(:running_status) { Idea::RunningStatus::RELEASED }

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
          expect(data['idea_reports']).to be_present
          expect(data['idea_reports'].size).to eq(2)
          expect(data['users']).to be_present
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(CommonConst::ACCESS_KEY_NAME.to_sym) { 'Bearer token="invalid_token_tsetadfdfads"' }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end
  end

  path '/api/v1/idea_reports/{id}' do
    get 'Show idea report' do
      tags 'IdeaReport'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      parameter name: :id,
        in: :path,
        type: :integer,
        required: true,
        description: 'Idea report unique id'

      let(:id) { 1 }

      response 200, 'Success' do
        examples 'application/json' => {
          code: 0,
          message: 'success',
          idea_report: MOCKED_DATA[:idea_reports].first,
          users: MOCKED_DATA[:users],
        }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
            idea_report: { '$ref': '#/components/schemas/idea_report' },
            users: {
              type: :array,
              items: { '$ref': '#/components/schemas/user' },
            },
          },
          required: %w[code message]

        let(:workspace) { create :workspace }
        let(:user) { create :user, workspace: workspace }
        let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

        let(:idea_definition) { create :idea_definition_with_questions, workspace: workspace, user: user }
        let(:idea) do
          create :idea_with_questions, workspace: workspace, idea_definition: idea_definition,
            running_status: Idea::RunningStatus::RELEASED
        end
        let!(:idea_participants) do # rubocop:disable RSpec/LetSetup
          create_list :idea_participant_with_question_answers, 3, workspace: workspace, idea: idea
        end
        let(:id) { idea.id }

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
          expect(data['idea_report']).to be_present
          expect(data['idea_report']['questions']).to be_present
          expect(data['users']).to be_present
          expect(data['users'].size).to eq(4)
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(CommonConst::ACCESS_KEY_NAME.to_sym) { 'Bearer token="invalid_token_tsetadfdfads"' }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end
  end
end
