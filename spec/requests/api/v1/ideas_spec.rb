# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'api/v1/ideas', type: :request do
  fixtures :oauth_applications

  let(:workspace) { create :workspace }
  # let(:user) { create :user, :with_slack_user, workspace: workspace }
  let(:user) { create :user, workspace: workspace }
  let(:slack_user) { user.slack_user }
  let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

  path '/api/v1/ideas' do
    get 'list ideas' do
      tags 'Idea'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      response 200, 'Success' do
        examples 'application/json' => {
          code: 0,
          message: 'success',
          ideas: [
            {
              id: 130,
              name: '2022-04-11 15:00:07 UTC - idea definition name 1',
              start_time: '2022-04-11T15:00:07.380Z',
              release_time: '2022-04-11T17:00:07.380Z',
              running_status: 'not_started',
              user: {
                id: 107,
                slack_user: {
                  ref_team_id: 'slack_team_id_1',
                  ref_user_id: 'ref_user_id_1',
                  email: 'email_1@slack_user.com',
                  profile: {
                    avatar_hash: 'email_1@slack_user.com',
                    email: 'email_1@slack_user.com',
                    real_name: 'email_1@slack_user.com',
                  },
                },
              },
            },
            {
              id: 133,
              name: '2022-04-11 15:00:07 UTC - idea definition name 2',
              start_time: '2022-04-11T15:00:07.430Z',
              release_time: '2022-04-11T17:00:07.430Z',
              running_status: 'not_started',
              user: {
                id: 108,
                slack_user: {
                  ref_team_id: 'slack_team_id_1',
                  ref_user_id: 'ref_user_id_2',
                  email: 'email_2@slack_user.com',
                  profile: {
                    avatar_hash: 'email_2@slack_user.com',
                    email: 'email_2@slack_user.com',
                    real_name: 'email_2@slack_user.com',
                  },
                },
              },
            },
          ],
        }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
            ideas: {
              type: :array,
              items: {
                '$ref': '#/components/schemas/idea_abbr',
              },
            },
          },
          required: %w[code message ideas]

        # Idea creator
        let(:idea_definitions) { create_list :idea_definition, created_idea_size, workspace: workspace, user: user }
        let!(:created_ideas) do # rubocop:disable RSpec/LetSetup
          ideas = idea_definitions.map do |idea_definition|
            create :idea_with_questions, workspace: workspace, idea_definition: idea_definition
          end
          ideas.first.update! invited_participants: invited_participants
          ideas
        end
        let(:created_idea_size) { 3 }

        # Idea participant
        let!(:joined_ideas) do # rubocop:disable RSpec/LetSetup
          create_list :idea_with_questions, joined_idea_size, workspace: workspace,
            invited_participants: invited_participants
        end
        let(:invited_participants) do
          [{
            category: Workspace::Category::SLACK,
            ref_team_id: slack_user.slack_team.ref_team_id,
            ref_user_id: slack_user.ref_user_id,
            email: slack_user.email,
            name: slack_user.profile['real_name'],
          }]
        end
        let(:joined_idea_size) { 4 }

        let!(:other_ideas) { create_list :idea_with_questions, other_idea_size, workspace: workspace } # rubocop:disable RSpec/LetSetup
        let(:other_idea_size) { 5 }

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
          expect(data['ideas'].size).to eq(created_idea_size + joined_idea_size)
        end
      end
    end
  end

  path '/api/v1/ideas/{id}' do
    parameter name: 'id', in: :path, type: :integer, required: true, description: 'id'

    get 'show idea' do
      tags 'Idea'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      response 200, 'Success' do
        examples 'application/json' => {
          code: 0,
          message: 'success',
          idea: {
            id: 1,
            name: 'Daily standup',
            description: 'daily standup feedback',
            start_time: '2022-02-01T12:00:00z',
            release_time: '2022-02-01T13:00:00z',
            timezone: CommonConst::ORIGIN_TIMEZONES.first,
            invited_participants: [
              { category: Workspace::Category::SLACK, ref_team_id: 'slack_team_id_1', ref_user_id: 'slack_user_id_1',
                email: 'user1@test.com', name: 'user1' },
              { category: Workspace::Category::SLACK, ref_team_id: 'slack_team_id_1', ref_user_id: 'slack_user_id_2',
                email: 'user2@test.com', name: 'user2' },
              { category: Workspace::Category::SLACK, ref_team_id: 'slack_team_id_1', ref_user_id: 'slack_user_id_3',
                email: 'user3@test.com', name: 'user3' },
            ],
            report_shareable: false,
            status: Idea::Status::ENABLED,
            running_status: Idea::RunningStatus::STARTED,
            questions: [
              {
                id: 1,
                ordinal: 0,
                content: '昨天吃了什么？',
                required: true,
              },
              {
                id: 2,
                ordinal: 1,
                content: '今天计划吃什么？',
                required: true,
              },
            ],
            answers: [
              {
                id: 1,
                idea_question_id: 1,
                content: '昨天吃了泡面+饺子',
                tags: %w[泡面 饺子],
                vote_user_ids: [1, 2, 3],
                comments: [
                  {
                    id: 1,
                    content: '吃泡面 +1',
                    user_id: 2,
                    created_at: '2022-01-01T12:00:00',
                  },
                  {
                    id: 2,
                    content: '吃泡面 +10086',
                    user_id: 3,
                    created_at: '2022-01-01T12:00:00',
                  },
                ],
              },
              {
                id: 2,
                idea_question_id: 2,
                content: '今天计划吃汤圆、火锅和开封菜',
                tags: %w[火锅 KFC],
                vote_user_ids: [1],
                comments: [],
              },
            ],
          },
          users: [
            {
              id: 1,
              slack_user: {
                ref_team_id: 'slack_team_id_1',
                ref_user_id: 'ref_user_id_1',
                email: 'email_1@slack_user.com',
                profile: {
                  avatar_hash: 'email_1@slack_user.com',
                  email: 'email_1@slack_user.com',
                  real_name: 'email_1@slack_user.com',
                },
              },
            },
            {
              id: 2,
              slack_user: {
                ref_team_id: 'slack_team_id_2',
                ref_user_id: 'ref_user_id_2',
                email: 'email_2@slack_user.com',
                profile: {
                  avatar_hash: 'email_2@slack_user.com',
                  email: 'email_2@slack_user.com',
                  real_name: 'email_2@slack_user.com',
                },
              },
            },
            {
              id: 3,
              slack_user: {
                ref_team_id: 'slack_team_id_3',
                ref_user_id: 'ref_user_id_3',
                email: 'email_3@slack_user.com',
                profile: {
                  avatar_hash: 'email_3@slack_user.com',
                  email: 'email_3@slack_user.com',
                  real_name: 'email_3@slack_user.com',
                },
              },
            },
          ],
        }

        schema type: :object,
          properties: {
            code: { type: :integer },
            message: { type: :string },
            idea: { '$ref': '#/components/schemas/idea_show' },
            users: {
              type: :array,
              items: { '$ref': '#/components/schemas/user' },
            },
          },
          required: %w[code message]

        let(:user) { create :user, workspace: workspace }
        let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

        let(:idea) { create :idea_with_questions, workspace: workspace, invited_participants: invited_participants }
        let(:id) { idea.id }
        let!(:idea_participant) { create :idea_participant_with_question_answers, idea: idea, user: user } # rubocop:disable RSpec/LetSetup

        let(:slack_user) { user.slack_user }
        let(:invited_participants) do
          [{
            category: Workspace::Category::SLACK,
            ref_team_id: slack_user.slack_team.ref_team_id,
            ref_user_id: slack_user.ref_user_id,
            email: slack_user.email,
            name: slack_user.profile['real_name'],
          }]
        end

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
          expect(data['idea']['id']).to eq(id)
          expect(data['idea']['answers']).to be_present
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(CommonConst::ACCESS_KEY_NAME.to_sym) { 'Bearer token="invalid_token_tsetadfdfads"' }
        let(:id) { 0 }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end
  end

  path '/api/v1/ideas/{id}/answer' do
    parameter name: 'id', in: :path, type: :integer, description: 'id'

    post 'submit idea answer' do
      tags 'Idea'
      consumes 'application/json'
      produces 'application/json'
      security [{ api_key: [] }]

      parameter name: :request_body,
        in: :body,
        required: true,
        schema: {
          type: :object,
          properties: {
            question_answer: {
              '$ref': '#/components/schemas/question_answer_input',
            },
          },
          required: [:question_answer],
        }

      let(:request_body) { { question_answer: question_answer } }
      let(:question_answer) do
        {
          idea_question_id: 1,
          content: 'Question answer',
          tags: [],
        }
      end

      response 200, 'Success' do
        examples 'application/json' => { code: 0, message: 'success' }

        schema type: :object,
          properties: { code: { type: :integer }, message: { type: :string } },
          required: %w[code message]

        let(:user) { create :user, workspace: workspace }
        let(:slack_user) { user.slack_user }
        let(CommonConst::ACCESS_KEY_NAME.to_sym) { "Bearer #{user.access_tokens.last.token}" }

        let(:idea) do
          create :idea_with_questions, workspace: workspace, invited_participants: invited_participants,
            running_status: Idea::RunningStatus::STARTED
        end
        let(:id) { idea.id }

        let(:invited_participants) do
          [{
            category: Workspace::Category::SLACK,
            ref_team_id: slack_user.slack_team.ref_team_id,
            ref_user_id: slack_user.ref_user_id,
            email: slack_user.email,
            name: slack_user.profile['real_name'],
          }]
        end

        let(:question_answer) do
          question = idea.questions.first

          {
            idea_question_id: question.id,
            content: "Question answer: #{question.content}",
            tags: [question.id.to_s],
          }
        end

        run_test! do |response|
          data = JSON.parse response.body
          expect(data['code']).to be_zero
        end
      end

      response 401, 'Access denied' do
        examples 'text/plain' => "HTTP Token: Access denied.\n"

        schema type: :string

        let(CommonConst::ACCESS_KEY_NAME.to_sym) { 'Bearer token="invalid_token_tsetadfdfads"' }
        let(:id) { 0 }

        run_test! do |response|
          expect(response.body).to eq("HTTP Token: Access denied.\n")
        end
      end
    end
  end
end
