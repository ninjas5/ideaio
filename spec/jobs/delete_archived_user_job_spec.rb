# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DeleteArchivedUserJob, type: :job do
  fixtures :oauth_applications

  describe '.perform' do
    subject { described_class.perform_now(user_id) }

    let(:user_id) { user.id }
    let(:user) { create :user, archived_at: archived_at }
    let(:archived_at) { CommonConst::ACCOUNT_DELETION_BUFFER.before }

    it 'success' do
      expect_any_instance_of(described_class).to receive(:anonymous_user).once.with(user).and_call_original

      subject

      latest_user = User.find(user_id)
      latest_user.reload

      expect(latest_user.email).to match(/\Aanonymous-.+@ideaio.xyz\z/)
      expect(latest_user.slack_user.profile['real_name']).to eq('Anonymous')
    end
  end
end
