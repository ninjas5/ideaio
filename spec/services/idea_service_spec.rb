# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IdeaService do
  fixtures :oauth_applications

  let(:workspace) { create :workspace }

  describe '.answer' do
    subject { described_class.answer(id, answer_params, user) }

    let(:idea) do
      create :idea_with_questions,
        workspace: workspace,
        invited_participants: invited_participants,
        running_status: Idea::RunningStatus::STARTED
    end
    let(:invited_participants) do
      [{
        category: Workspace::Category::SLACK,
        ref_team_id: user.slack_user.slack_team.ref_team_id,
        ref_user_id: user.slack_user.ref_user_id,
      }]
    end
    let(:user) { create :user, workspace: workspace }
    let(:id) { idea.id }
    let(:answer_params) do
      question = idea.questions.first

      { idea_question_id: question.id, content: "Answer for #{question.content}", tags: ['test'] }
    end

    it 'success' do
      expect(idea.idea_participants).to be_empty
      subject
      idea.reload
      expect(idea.idea_participants).to be_present
    end

    context 'when update answer' do
      let(:idea_participant) do
        create :idea_participant_with_question_answers, workspace: workspace, idea: idea, user: user
      end
      let(:answer_params) do
        answer = idea_participant.question_answers.first

        answer.slice(:idea_question_id).merge(
          content: "Answer for qeustion-#{answer.idea_question_id}",
          tags: tags,
        )
      end
      let(:tags) { %w[test tags test-tag] }

      it 'success' do
        idea_participant.question_answers.each do |answer|
          expect(answer.tags).not_to eq(tags)
        end

        subject

        idea_participant.reload
        expect(idea_participant.question_answers.first.tags).to eq(tags)
      end
    end
  end
end
