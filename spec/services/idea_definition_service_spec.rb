# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IdeaDefinitionService do
  fixtures :oauth_applications

  let(:workspace) { create :workspace }
  let(:creator) { create :user, workspace: workspace }

  describe '.archive' do
    subject { described_class.archive(user, idea_definition.id) }

    let!(:idea_definition) { create :idea_definition, workspace: workspace, user: creator }
    let(:user) { creator }

    it 'success' do
      subject
      idea_definition.reload
      expect(idea_definition).to be_archived
    end

    context 'with not creator' do
      let(:user) { create :user, workspace: workspace }

      it 'not archived' do
        expect { subject }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe '.scoped_resources' do
    subject { described_class.scoped_resources(user) }

    let(:user) { creator }
    let!(:created_idea_definition) { create :idea_definition, workspace: workspace, user: creator }
    let!(:joined_idea_definition_not_started) do # rubocop:disable RSpec/LetSetup
      create(
        :idea_definition,
        workspace: workspace,
        user: another,
        invited_participants: invited_participants,
      )
    end
    let!(:joined_idea_definition) do
      create(
        :idea_definition,
        workspace: workspace,
        user: another,
        invited_participants: invited_participants,
      )
    end
    let(:another) { create :user, workspace: workspace }
    let!(:not_joined_idea_definition) { create :idea_definition, workspace: workspace, user: another } # rubocop:disable RSpec/LetSetup

    let!(:joined_idea) do
      create(
        :idea,
        workspace: workspace,
        idea_definition: joined_idea_definition,
        invited_participants: joined_idea_definition.invited_participants,
        running_status: Idea::RunningStatus::STARTED,
      )
    end

    let(:invited_participants) do
      [
        {
          category: Workspace::Category::SLACK,
          ref_team_id: user.slack_user.slack_team.ref_team_id,
          ref_user_id: user.slack_user.ref_user_id,
          email: user.email,
          name: user.slack_user.profile['real_name'],
        },
      ]
    end

    it 'success' do
      expect(subject.ids).to contain_exactly(created_idea_definition.id, joined_idea_definition.id)
      subject.each do |idea_definition|
        if idea_definition.id.eql?(joined_idea_definition.id)
          expect(idea_definition.singleton_idea.id).to eq(joined_idea.id)
        else
          expect(idea_definition.singleton_idea).to be_nil
        end
      end
    end
  end
end
