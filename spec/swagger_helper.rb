# frozen_string_literal: true

require 'rails_helper'

RSpec.configure do |config|
  # Specify a root folder where Swagger JSON files are generated
  # NOTE: If you're using the rswag-api to serve API descriptions, you'll need
  # to ensure that it's configured to serve Swagger from the same folder
  config.swagger_root = Rails.root.join('swagger').to_s

  # Define one or more Swagger documents and provide global metadata for each one
  # When you run the 'rswag:specs:swaggerize' rake task, the complete Swagger will
  # be generated at the provided relative path under swagger_root
  # By default, the operations defined in spec files are added to the first
  # document below. You can override this behavior by adding a swagger_doc tag to the
  # the root example_group in your specs, e.g. describe '...', swagger_doc: 'v2/swagger.json'
  config.swagger_docs = {
    'v1/swagger.yaml' => {
      openapi: '3.0.1',
      info: {
        title: 'API V1',
        version: 'v1',
      },
      paths: {},
      servers: [
        {
          url: '{protocol}://{host}',
          variables: {
            protocol: {
              default: 'https',
              enum: %i[http https],
            },
            host: {
              default: 'app.ideaio.xyz',
            },
          },
        },
      ],
      components: {
        securitySchemes: {
          # basic_auth: {
          #   type: :http,
          #   scheme: :basic,
          # },
          api_key: {
            type: :apiKey,
            in: :header,
            name: CommonConst::ACCESS_KEY_NAME,
          },
        },
        schemas: {
          big_decimal: { type: :string, pattern: '^(?:[1-9][0-9]*|0)(?:\.[0-9]*)?$' },
          time_24h: { type: :string, pattern: '^(?:[0-1][0-9]|2[0-3])(?::[0-5][0-9]){1,2}$' },
          datetime_without_tz: { type: :string,
                                 pattern: CommonConst::IdeaioRegexp::DATETIME_WITHOUT_TZ },
          display_datetime: { type: :string, pattern: CommonConst::IdeaioRegexp::DISPLAY_DATETIME },
          external_user_category: { type: :string, enum: Workspace::Category.const_values },
          idea_template_category: { type: :string, enum: IdeaTemplate::Category.const_values },
          #  pattern: '^\d{4}-\d{2}-\d{2} (?:[0-1][0-9]|2[0-3])(?::[0-5][0-9]){1,2}$' },
          result_obj: {
            type: :object,
            properties: {
              code: { type: :integer },
              message: { type: :string },
            },
            required: %i[code message],
          },
          workspace: {
            type: :object,
            properties: {
              id: { type: :integer },
              category: { '$ref': '#/components/schemas/external_user_category' },
              name: { type: :string },
              archived_at: { type: [:string, nil] },
              created_at: { type: :string },
              updated_at: { type: :string },
            },
            required: %i[id category name created_at updated_at],
          },
          idea_template: {
            type: :object,
            properties: {
              id: { type: :integer },
              workspace_id: { type: [:integer, nil] },
              user_id: { type: [:integer, nil] },
              category: { '$ref': '#/components/schemas/idea_template_category' },
              name: { type: :string },
              description: { type: :string },
            },
            required: %i[id category name description],
          },
          question: {
            type: :object,
            properties: {
              id: { type: :integer },
              ordinal: { type: :integer },
              content: { type: :string },
            },
            required: %i[id ordinal content],
          },
          idea_template_question: { '$ref': '#/components/schemas/question' },
          timer_trigger: {
            type: :object,
            properties: {
              ordinal: { type: :integer },
              start_time: { '$ref': '#/components/schemas/time_24h' },
              period_time: { type: :string },
            },
            required: %i[ordinal start_time period_time],
          },
          cycle_timer: {
            type: :object,
            properties: {
              cycle_type: { type: :string },
              cycle_count: { type: :integer },
              start_date: { type: :string },
              end_date: { type: [:string, nil] },
              timer_triggers: {
                type: :array,
                items: { '$ref': '#/components/schemas/timer_trigger' },
              },
            },
            required: %i[cycle_type cycle_count timer_triggers start_date],
          },
          single_timer: {
            type: :object,
            properties: {
              start_time: { '$ref': '#/components/schemas/datetime_without_tz' },
              release_time: { '$ref': '#/components/schemas/datetime_without_tz' },
            },
            required: %i[start_time release_time],
          },
          invited_channel: {
            type: :object,
            properties: {
              category: { '$ref': '#/components/schemas/external_user_category' },
              channel_id: { type: :string },
              name: { type: :string },
              ref_team_id: { type: :string },
            },
            required: %i[category channel_id name ref_team_id],
          },
          invited_participant: {
            type: :object,
            properties: {
              category: { '$ref': '#/components/schemas/external_user_category' },
              email: { type: :string },
              name: { type: :string },
              ref_team_id: { type: :string },
              ref_user_id: { type: :string },
            },
            required: %i[category email name ref_team_id ref_user_id],
          },
          idea_definition_abbr: {
            type: :object,
            properties: {
              id: { type: :integer },
              user_id: { type: :integer },
              name: { type: :string },
              running_status: { type: :string },
              start_time: { type: :string },
              release_time: { type: :string },
              idea_id: { type: [:integer, nil] },
            },
            required: %i[id user_id name running_status start_time release_time],
          },
          idea_definition: {
            type: :object,
            properties: {
              id: { type: :integer },
              workspace_id: { type: :integer },
              user_id: { type: :integer },
              idea_template_id: { type: [:integer, nil] },
              name: { type: :string },
              description: { type: :string },
              # status: { type: :string, enum: IdeaDefinition::Status.const_values },
              timezone: { type: :string },
              # repeatable: { type: :boolean },
              timer: { '$ref' => '#/components/schemas/idea_timer' },
              channels: {
                type: :array,
                items: { '$ref' => '#/components/schemas/invited_channel' },
              },
              invited_participants: {
                type: :array,
                items: { '$ref' => '#/components/schemas/invited_participant' },
              },
              report_shareable: { type: :boolean },
              archived_at: { type: [:string, nil] },
              questions: {
                type: :array,
                items: { '$ref' => '#/components/schemas/idea_question_definition' },
              },
            },
            required: %i[id workspace_id user_id name description timezone timer channels
                         invited_participants report_shareable questions],
          },
          idea_timer: {
            anyOf: [
              { '$ref' => '#/components/schemas/single_timer' },
              # { '$ref' => '#/components/schemas/cycle_timer' }, # TODO: 暂时不支持
            ],
          },
          idea_timer_input: {
            type: :object,
            properties: {
              start_time: { '$ref': '#/components/schemas/datetime_without_tz' },
              release_time: { '$ref': '#/components/schemas/datetime_without_tz' },
            },
            required: %i[start_time release_time],
          },
          idea_definition_input: {
            type: :object,
            properties: {
              idea_definition: {
                type: :object,
                properties: {
                  idea_template_id: { type: [:integer, nil] },
                  name: { type: :string },
                  description: { type: :string },
                  timezone: { type: :string },
                  timer: { '$ref' => '#/components/schemas/idea_timer_input' },
                  channels: {
                    type: :array,
                    items: { '$ref' => '#/components/schemas/invited_channel' },
                  },
                  invited_participants: {
                    type: :array,
                    items: { '$ref' => '#/components/schemas/invited_participant' },
                  },
                  report_shareable: { type: :boolean, default: false },
                  questions: {
                    type: :array,
                    items: {
                      type: :object,
                      properties: {
                        content: { type: :string },
                      },
                      required: %i[content],
                    },
                  },
                },
                required: %i[idea_template_id name description timezone timer channels invited_participants
                             report_shareable questions],
              },
            },
            required: %i[idea_definition],
          },
          idea_question_definition: { '$ref' => '#/components/schemas/question' },
          idea_abbr: {
            type: :object,
            properties: {
              id: { type: :integer },
              name: { type: :string },
              running_status: { type: :string },
              user: { '$ref' => '#/components/schemas/user' },
              start_time: { type: :string },
              release_time: { type: :string },
            },
            required: %i[id name running_status user start_time release_time],
          },
          idea_show: {
            type: :object,
            properties: {
              id: { type: :integer },
              user_id: { type: :integer },
              name: { type: :string },
              description: { type: :string },
              start_time: { type: :string },
              release_time: { type: :string },
              timezone: { type: :string },
              invited_participants: {
                type: :array,
                items: { '$ref' => '#/components/schemas/invited_participant' },
              },
              report_shareable: { type: :boolean },
              status: { type: :string, enum: Idea::Status.const_values },
              running_status: { type: :string },
              questions: {
                type: :array,
                items: { '$ref' => '#/components/schemas/idea_question' },
              },
              answers: {
                type: :array,
                items: { '$ref' => '#/components/schemas/question_answer' },
              },
            },
            required: %i[id user_id name description start_time release_time timezone invited_participants
                         report_shareable status running_status questions],
          },
          idea_question: { '$ref' => '#/components/schemas/question' },
          question_answer_input: {
            type: :object,
            properties: {
              idea_question_id: { type: :integer },
              content: { type: :string },
              tags: { type: :array, items: { type: :string } },
            },
            required: %i[idea_question_id content tags],
          },
          question_comment: {
            type: :object,
            properties: {
              id: { type: :integer },
              content: { type: :string },
              user_id: { type: :integer },
              created_at: { type: :string },
            },
            required: %i[id content user_id created_at],
          },
          question_answer: {
            type: :object,
            properties: {
              id: { type: :integer },
              idea_question_id: { type: :integer },
              content: { type: :string },
              tags: { type: :array, items: { type: :string } },
              vote_user_ids: { type: :array, items: { type: :integer } },
              created_at: { type: :string },
              updated_at: { type: :string },
              comments: {
                type: :array,
                items: { '$ref' => '#/components/schemas/question_comment' },
              },
            },
            required: %i[id idea_question_id content tags vote_user_ids created_at updated_at],
          },
          report_question: {
            type: :object,
            properties: {
              id: { type: :integer },
              ordinal: { type: :integer },
              content: { type: :string },
              answers: {
                type: :array,
                items: {
                  type: :object,
                  properties: {
                    id: { type: :integer },
                    content: { type: :string },
                    tags: { type: :array, items: { type: :string } },
                    vote_user_ids: { type: :array, items: { type: :integer } },
                    created_at: { '$ref' => '#/components/schemas/display_datetime' },
                    updated_at: { '$ref' => '#/components/schemas/display_datetime' },
                    comments: {
                      type: :array,
                      items: { '$ref' => '#/components/schemas/question_comment' },
                    },
                  },
                  required: %i[id content tags vote_user_ids created_at updated_at],
                },
              },
            },
            required: %i[id ordinal content answers],
          },
          idea_answer: {
            type: :object,
            properties: {
              id: { type: :integer },
              user_id: { type: :integer },
              question_answers: {
                type: :array,
                items: { '$ref' => '#/components/schemas/question_answer' },
              },
            },
            required: %i[id user_id question_answers],
          },
          idea_report_abbr: {
            type: :object,
            properties: {
              id: { type: :integer },
              user_id: { type: :integer },
              name: { type: :string },
              description: { type: :string },
              start_time: { type: :string },
              release_time: { type: :string },
              timezone: { type: :string, enum: CommonConst::ORIGIN_TIMEZONES },
              report_shareable: { type: :boolean },
            },
            required: %i[id user_id name description start_time release_time timezone report_shareable],
          },
          idea_report: {
            type: :object,
            properties: {
              id: { type: :integer },
              name: { type: :string },
              description: { type: :string },
              start_time: { '$ref' => '#/components/schemas/display_datetime' },
              release_time: { '$ref' => '#/components/schemas/display_datetime' },
              questions: {
                type: :array,
                items: { '$ref' => '#/components/schemas/report_question' },
              },
            },
            required: %i[id name description start_time release_time questions],
          },
          slack_user_profile: {
            type: :object,
            properties: {
              avatar_hash: { type: :string },
              status_text: { type: :string },
              status_emoji: { type: :string },
              status_expiration: { type: :integer },
              real_name: { type: :string },
              display_name: { type: :string },
              real_name_normalized: { type: :string },
              display_name_normalized: { type: :string },
              email: { type: :string },
              image_original: { type: :string },
              image_24: { type: :string },
              image_32: { type: :string },
              image_48: { type: :string },
              image_72: { type: :string },
              image_192: { type: :string },
              image_512: { type: :string },
              team: { type: :string },
            },
            required: %i[avatar_hash email real_name],
          },
          slack_user: {
            type: :object,
            properties: {
              ref_team_id: { type: :string },
              ref_user_id: { type: :string },
              email: { type: :string },
              profile: { '$ref' => '#/components/schemas/slack_user_profile' },
            },
            required: %i[ref_team_id ref_user_id email profile],
          },
          user: {
            type: :object,
            properties: {
              id: { type: :integer },
              slack_user: { '$ref' => '#/components/schemas/slack_user' },
            },
            required: %i[id slack_user],
          },
        },
      },
    },
  }

  config.swagger_format = :yaml
end
