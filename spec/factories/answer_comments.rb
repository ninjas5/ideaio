# frozen_string_literal: true

FactoryBot.define do
  factory :answer_comment do
    workspace
    user { association :user, workspace: workspace }
    question_answer { association :question_answer, workspace: workspace }
    content { 'Good good study, day day up!!!' }
  end
end
