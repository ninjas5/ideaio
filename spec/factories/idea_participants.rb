# frozen_string_literal: true

FactoryBot.define do
  factory :idea_participant do
    workspace
    idea { association :idea, workspace: workspace }
    user { association :user, workspace: workspace }

    factory :idea_participant_with_question_answers, class: 'IdeaParticipant' do
      idea { association :idea_with_questions, workspace: workspace }

      after(:create) do |idea_participant|
        idea_participant.idea.questions.each do |question|
          create :question_answer,
            workspace: idea_participant.workspace,
            idea_participant: idea_participant,
            idea_question: question
        end
      end
    end
  end
end
