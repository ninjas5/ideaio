# frozen_string_literal: true

FactoryBot.define do
  factory :idea_definition do
    workspace
    user { association :user, workspace: workspace }
    idea_template { association :idea_template, workspace: workspace, user: user }
    sequence(:name) { |n| "idea definition name #{n}" }
    description { "#{name} description" }
    status { IdeaDefinition::Status::ENABLED }
    repeatable { false }
    timezone { CommonConst::ORIGIN_TIMEZONES.sample }
    timer do
      {
        start_time: 10.minutes.after.in_time_zone(timezone).strftime('%Y-%m-%d %H:%M:%S'),
        release_time: 1.hour.after.in_time_zone(timezone).strftime('%Y-%m-%d %H:%M:%S'),
      }
    end
    channels do
      [
        { category: Workspace::Category::SLACK, ref_team_id: 'team_id_1', channel_id: 'channel_id_1', name: 'general' },
        { category: Workspace::Category::SLACK, ref_team_id: 'team_id_1', channel_id: 'channel_id_2', name: 'fun' },
      ]
    end
    invited_participants do
      [
        { category: Workspace::Category::SLACK, ref_team_id: 'team_id_1', ref_user_id: 'slack_user_id_1',
          email: 'user1@test.com', name: 'user1' },
        { category: Workspace::Category::SLACK, ref_team_id: 'team_id_1', ref_user_id: 'slack_user_id_2',
          email: 'user2@test.com', name: 'user2' },
        { category: Workspace::Category::SLACK, ref_team_id: 'team_id_1', ref_user_id: 'slack_user_id_3',
          email: 'user3@test.com', name: 'user3' },
      ]
    end
    report_shareable { true }
    archived_at { nil }

    factory :repeatable_idea_definition, class: 'IdeaDefinition' do
      repeatable { true }
      timer do
        {
          cycle: '1.weeks',
          triggers: [
            { ordinal: 0, start_time: '09:00:00', period_time: 7_200 },
            { ordinal: 1, start_time: '09:00:00', period_time: 7_200 },
            { ordinal: 2, start_time: '09:00:00', period_time: 7_200 },
            { ordinal: 3, start_time: '09:00:00', period_time: 7_200 },
            { ordinal: 4, start_time: '09:00:00', period_time: 7_200 },
          ],
        }
      end
    end

    factory :idea_definition_with_questions, class: 'IdeaDefinition' do
      after(:create) do |idea_definition|
        create_list :idea_question_definition, 3, idea_definition: idea_definition
      end
    end
  end
end
