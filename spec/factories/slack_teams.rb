# frozen_string_literal: true

FactoryBot.define do
  factory :slack_team do
    workspace
    sequence(:ref_team_id) { |n| "slack_team_id_#{n}" }
    sequence(:ref_team_name) { |n| "slack team name #{n}" }
    scope { '' }
    sequence(:access_token) { |n| "slack_team_access_token_#{n}" }
    sequence(:refresh_token) { |n| "slack_team_refresh_token_#{n}" }
    expires_in { 3600 }
  end
end
