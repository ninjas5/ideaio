# frozen_string_literal: true

FactoryBot.define do
  factory :slack_user do
    workspace { user.workspace }
    slack_team { workspace.slack_team || association(:slack_team, workspace: workspace) }
    user
    sequence(:ref_user_id) { |n| "ref_user_id_#{n}" }
    sequence(:email) { |n| "email_#{n}@slack_user.com" }
    scope { '' }
    token_type { 'user' }
    sequence(:access_token) { |n| "access_token_#{n}" }
    sequence(:refresh_token) { |n| "refresh_token_#{n}" }
    expires_in { 3600 }
    sequence(:profile) { |n| { avatar_hash: email.hash.to_s, email: email, real_name: "Real Name #{n}" } }
  end
end
