# frozen_string_literal: true

FactoryBot.define do
  factory :idea_template do
    workspace
    user { association :user, workspace: workspace }
    category { IdeaTemplate::Category::GENERAL }
    sequence(:name) { |n| "idea template name #{n}" }
    description { "#{name} description" }
    status { IdeaTemplate::Status::ENABLED }

    factory :idea_template_with_questions, class: 'IdeaTemplate' do
      after(:create) do |template|
        create_list :idea_template_question, 3, idea_template: template
      end
    end
  end
end
