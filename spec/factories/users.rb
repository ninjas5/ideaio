# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    workspace
    sequence(:email) { |n| "user_#{n}@test.com" }
    roles { [User::Role::ADMIN] }
    archived_at { nil }

    after(:build) do |user|
      build :slack_user, workspace: user.workspace, user: user
    end

    after(:create) do |user|
      application = Doorkeeper::Application.find_by!(name: CommonConst::OAuthApplication::IDEAIO)

      user.access_grants.create!(
        application: application,
        redirect_uri: application.redirect_uri,
        scopes: application.scopes,
        expires_in: CommonConst::ACCESS_GRANT_EXPIRES_IN,
      )

      user.access_tokens.create!(
        application: application,
        scopes: application.scopes,
        expires_in: CommonConst::ACCESS_TOKEN_EXPIRES_IN,
        use_refresh_token: true,
      )
    end
  end
end
