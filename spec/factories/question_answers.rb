# frozen_string_literal: true

FactoryBot.define do
  factory :question_answer do
    workspace
    idea_participant { association :idea_participant, workspace: workspace }
    idea_question { association :idea_question, idea: idea_participant.idea }
    content { "#{idea_question.content}: answer content" }
    tags { ['tech', 'ai', 'big data'] }
    attachments { [] }
    vote_user_ids { [idea_participant.user_id] }
  end
end
