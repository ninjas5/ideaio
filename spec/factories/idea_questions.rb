# frozen_string_literal: true

FactoryBot.define do
  factory :idea_question do
    idea
    sequence(:ordinal) { |n| n }
    content { "idea question #{ordinal}'th" }
    required { true }
  end
end
