# frozen_string_literal: true

FactoryBot.define do
  factory :workspace do
    category { Workspace::Category::SLACK }
    sequence(:name) { |n| "test workspace #{n}" }
    archived_at { nil }

    after(:build) do |workspace|
      build :slack_team, workspace: workspace
    end
  end
end
