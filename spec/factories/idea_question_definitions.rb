# frozen_string_literal: true

FactoryBot.define do
  factory :idea_question_definition do
    idea_definition
    sequence(:ordinal) { |n| n }
    sequence(:content) { "Idea question definition #{ordinal}" }
    required { true }
  end
end
