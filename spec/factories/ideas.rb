# frozen_string_literal: true

FactoryBot.define do
  factory :idea do
    workspace
    idea_definition { association :idea_definition, workspace: workspace }
    name { "#{Time.zone.now} - #{idea_definition.name}" }
    description { "#{name} description" }
    start_time { Time.zone.now }
    release_time { start_time + 2.hours }
    timezone { CommonConst::ORIGIN_TIMEZONES.sample }
    invited_participants do
      [
        { category: Workspace::Category::SLACK, ref_team_id: 'slack_team_1', ref_user_id: 'slack_user_1' },
        { category: Workspace::Category::SLACK, ref_team_id: 'slack_team_1', ref_user_id: 'slack_user_2' },
        { category: Workspace::Category::SLACK, ref_team_id: 'slack_team_1', ref_user_id: 'slack_user_3' },
      ]
    end
    report_shareable { true }
    status { Idea::Status::ENABLED }
    running_status { Idea::RunningStatus::STARTED }

    factory :idea_with_questions, class: 'Idea' do
      after(:create) do |idea|
        create_list :idea_question, 3, idea: idea
      end
    end
  end
end
