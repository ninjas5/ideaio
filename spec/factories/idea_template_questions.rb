# frozen_string_literal: true

FactoryBot.define do
  factory :idea_template_question do
    idea_template
    sequence(:ordinal) { |n| n }
    sequence(:content) { |n| "this is the #{n}'th questions" }
    required { true }
  end
end
