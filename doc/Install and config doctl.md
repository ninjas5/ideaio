
# Set up

```Bash
brew install doctl
```

```Bash
doctl auth init
doctl auth init --context <NAME>

# Verify
doctl account get
```

```Bash
doctl registry login

-> Logging Docker in to registry.digitalocean.com
```

# Related Docs

- [How to Install and Configure doctl](https://docs.digitalocean.com/reference/doctl/how-to/install/)
- [How to Create a Personal Access Token](https://docs.digitalocean.com/reference/api/create-personal-access-token/)
