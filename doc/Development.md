# Development

## Dependencies

- Ruby, **required**, '2.7.4'
- Rails, **required**, '6.1.5'
- Postgresql, **required**, '>= 12'
- Docker, **required**

### Ruby

- [rvm](http://rvm.io/). RVM is maintained by community of volunteers, report issues to RVM issues tracker.
If you can help or wish to become one of the maintainers - just start helping. You can find more RVM related projects at RVM Github organization.
- [Ruby]. A dynamic, open source programming language with a focus on simplicity and productivity. It has an elegant syntax that is natural to read and easy to write.
  - [Install Ruby using rvm](http://rvm.io/rubies/installing)

```bash
rvm install 2.7.4
rvm use 2.7.4
```

- Install bundler and gems

```bash
cd <project_path>
gem install bundler
bundle install
```

### Database

#### Init Postgresql in Mac

- [homebrew](https://brew.sh/)
- [postgresql](https://www.postgresql.org/download/macosx/), **required**

```bash
brew install postgresql
```

```bash
brew servcies list
brew servcies start postgresql
brew servcies stop postgresql
brew servcies restart postgresql
```

[Create new user](https://www.codementor.io/@engineerapart/getting-started-with-postgresql-on-mac-osx-are8jcopb)

```bash
psql postgres
```

```sql
CREATE ROLE username WITH LOGIN PASSWORD 'quoted password'
```

#### Init Postgresl in Ubuntu

```bash
sudo apt update
sudo apt install -y postgresql redis-server
```

```bash
sudo systemctl restart postgresql
sudo systemctl start postgresql
sudo systemctl stop postgresql
sudo systemctl status postgresql

sudo /etc/init.d/postgresql restart
sudo /etc/init.d/postgresql start
sudo /etc/init.d/postgresql stop
sudo /etc/init.d/postgresql status
```

```bash
sudo sed -Ei 's/(local +all +postgres +)[a-z]*$/\1trust/g' /etc/postgresql/*/main/pg_hba.conf
sudo sed -Ei 's/(local +all +all +)[a-z]$/\1md5/g' /etc/postgresql/*/main/pg_hba.conf
sudo /etc/init.d/postgresql restart
psql -U postgres -c "ALTER USER postgres with password 'postgres';"
```

## Database

```bash
bundle exec rails db:create
bundle exec rails db:migrate
```

> Load [seeds](../db/seeds.rb) data

```bash
bundle exec rails db:seed
```

> [Reset test data to database](../lib/tasks/backfills/dev/reset_db_data_in_dev_env.rake)

```bash
backfills:dev:reset_db_data_in_dev_env
```

## Run

**Run in local dev**

```bash
# Run Rails App
bundle exec rails s

# Health
open http://localhost:3000
open http://localhost:3000/ping

# Generate API doc
bundle exec rails rswag:specs:swaggerize
```

```bash
bundle exec rails c
bundle exec rails db -p
bundle exec rails r "Rails.logger.info('Hello World!!!')"
```

**Run test**

```bash
bundle exec RAILS_ENV=test rails db:create
bundle exec RAILS_ENV=test rails db:migrate

# Run unit test
bundle exec rspec spec

# Ruby code style checking and formatting
bundle exec rubocop -A .
```
