# Docker

## Build

```
scripts/docker_build.sh staging
scripts/docker_build.sh production
```

## Publish Docker to [Artifact Registry](https://console.cloud.google.com/gcr/images/valid-heuristic-346114?project=valid-heuristic-346114)

### Init gcloud

- [Install gcloud](https://cloud.google.com/sdk/docs/install-sdk)
- [Set Docker Authentication](https://cloud.google.com/artifact-registry/docs/docker/authentication)
- [Pull and push image](https://cloud.google.com/artifact-registry/docs/docker/pushing-and-pulling)

```
# Set permission
gcloud projects add-iam-policy-binding valid-heuristic-346114 \
    --member=user:skyxx2083@gmail.com --role=roles/artifactregistry.repoAdmin
gcloud projects add-iam-policy-binding valid-heuristic-346114 \
    --member=user:skyxx2083@gmail.com --role=roles/storage.admin

gcloud auth login
gcloud auth configure-docker us-docker.pkg.dev
```

### Push & pull

```
docker push us-docker.pkg.dev/valid-heuristic-346114/ideaio/ideaio-staging:base
docker push us-docker.pkg.dev/valid-heuristic-346114/ideaio/ideaio-staging:20220405161136
docker push us-docker.pkg.dev/valid-heuristic-346114/ideaio/ideaio-staging

docker pull us-docker.pkg.dev/valid-heuristic-346114/ideaio/ideaio-staging
```

### Run

#### Staging

```bash
docker run --name ideaio_staging --add-host=host.docker.internal:host-gateway \
    -m 1024M --memory-swap -1 -p 10001:80 -itd \
    -v /opt/app/ideaio/log:/app/log
    us-docker.pkg.dev/valid-heuristic-346114/ideaio/ideaio-staging

docker run --add-host=host.docker.internal:host-gateway \
    -m 1024M --memory-swap -1 -p 10001:80 -it \
    us-docker.pkg.dev/valid-heuristic-346114/ideaio/ideaio-staging bash
```

#### Production

```bash
docker run --name ideaio_production --add-host=host.docker.internal:host-gateway \
    -m 2048M --memory-swap -1 -p 10000:80 --env-file=.ideaio_prd_env -it \
    -v /opt/app/ideaio/log:/app/log
    us-docker.pkg.dev/valid-heuristic-346114/ideaio/ideaio

docker run --add-host=host.docker.internal:host-gateway \
    -m 2048M --memory-swap -1 -p 10000:80 --env-file=.ideaio_prd_env -it \
    us-docker.pkg.dev/valid-heuristic-346114/ideaio/ideaio bash
```
