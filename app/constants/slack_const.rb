# frozen_string_literal: true

module SlackConst
  module GrantType
    AUTHORIZATION_CODE = 'authorization_code'
    REFRESH_TOKEN = 'refresh_token'
  end

  DEFAULT_RETRY_TIMES = 2

  module IdeaioBot
    CONFIG = YAML.safe_load(ERB.new(File.read(Rails.root.join('config/slack.yml.erb'))).result)[Rails.env]

    SCOPE = %w[
      channels:read
      chat:write
      chat:write.public
      team:read
      users.profile:read
      users:read
      users:read.email
    ].freeze

    USER_SCOPE = %w[
      channels:read
      users.profile:read
      users:read
      users:read.email
    ].freeze
  end
end
