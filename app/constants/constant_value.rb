# frozen_string_literal: true

module ConstantValue
  def const_values
    constants.map { |item| const_get(item) }
  end
end
