# frozen_string_literal: true

module CommonConst
  # Set token in headers
  # Authorization: Bearer "your_token"
  # Authorization: Bearer your_token
  # Authorization: Bearer token="your_token"
  # Authorization: Bearer token=your_token
  # Authorization: Token token="your_token"
  # Authorization: Token token=your_token
  # Authorization: Token "your_token"
  # Authorization: Token your_token
  ACCESS_KEY_NAME = 'Authorization'

  IDEA_HOSTS = {
    'development' => 'https://80b3-220-241-205-25.ap.ngrok.io',
    'test' => 'http://test.ideaio.xyz',
    'staging' => 'https://staging.ideaio.xyz',
    'production' => 'https://app.ideaio.xyz',
  }.freeze

  IDEAIO_BASE_URL = IDEA_HOSTS[Rails.env]

  SLACK_AUTH_URL = "#{IDEAIO_BASE_URL}/user/login"

  ORIGIN_TIMEZONES = ActiveSupport::TimeZone::MAPPING.values.uniq

  now_time = Time.zone.now
  TIMEZONES = ORIGIN_TIMEZONES.map do |timezone|
    zonetime = now_time.in_time_zone(timezone)
    [
      zonetime.strftime('%z').to_i,
      { timezone: timezone, display_name: "#{Time.find_zone(timezone).now.strftime('(GMT%:z)')} #{timezone}" },
    ]
  end.sort_by(&:first).map(&:last)

  # rubocop:disable Layout/LineLength
  module IdeaioRegexp
    DATETIME_WITHOUT_TZ = /^\d{4}-(?:0?[1-9]|1[0-2])-(?:0?[1-9]|[12][0-9]|3[01]) (?:[0-1][0-9]|2[0-3])(?::[0-5][0-9]){1,2}$/.freeze
    DISPLAY_DATETIME = /^(?:0?[1-9]|1[0-2])-(?:0?[1-9]|[12][0-9]|3[01])-\d{4} (?:[0-1][0-9]|2[0-3]):[0-5][0-9] [AP]M$/i.freeze
  end
  # rubocop:enable Layout/LineLength

  module OAuthApplication
    IDEAIO = 'ideaio'
  end

  # TODO: @emma, 设置了30天有效期，可以随时调整
  ACCESS_GRANT_EXPIRES_IN = 30.days
  ACCESS_TOKEN_EXPIRES_IN = 30.days

  # 删除账户之后的缓冲期，在删除账户之后的一段时间之内登录系统，将会重新激活该账户，否则将会在到期之后删除账户数据
  ACCOUNT_DELETION_BUFFER = Rails.env.production? ? 90.days : 1.day
end
