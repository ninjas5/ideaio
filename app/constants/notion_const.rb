# frozen_string_literal: true

module NotionConst
  module IdeaioBot
    CLIENT_ID = ENV.fetch('IDEAIO_NOTION_CLIENT_ID', 'mock')
    CLIENT_SECRET = ENV.fetch('IDEAIO_NOTION_CLIENT_SECRET', 'mock')

    REDIRECT_URI = '/settings'
    NOTION_HOST = 'https://api.notion.com'
    AUTH_URI = "#{NOTION_HOST}/v1/oauth/authorize"
  end
end
