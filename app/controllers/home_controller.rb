# frozen_string_literal: true

class HomeController < ApplicationController
  def index
    render plain: 'Hello Ideaio!!!'
  end

  def ping
    render plain: 'Pong'
  end
end
