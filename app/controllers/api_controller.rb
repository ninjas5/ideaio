# frozen_string_literal: true

class ApiController < ActionController::API
  include ActionController::HttpAuthentication::Token::ControllerMethods

  before_action :doorkeeper_authorize!
  after_action :log_output_body

  rescue_from StandardError, with: :handle_standard_error

  private

  def log_output_body
    logger.info { "Successful process and reponse: #{response.body}" }
  end

  def handle_standard_error(exception)
    case exception
    when Doorkeeper::Errors::InvalidToken
      request_http_token_authentication
    when ActiveRecord::ActiveRecordError
      logger.error(exception)
      render_json code: 1001, message: exception.message
    when Slack::Web::Api::Errors::SlackError
      logger.error(exception)
      render_json code: 1002, message: exception.message
    else
      logger.error(exception)
      render_json code: 1000, message: exception.message
    end
  end

  def handle_invalid_token(exception)
    logger.error(exception)
    request_http_token_authentication
  end

  def render_json(code: 0, message: 'success', **args)
    render json: args.merge(code: code, message: message)
  end

  def doorkeeper_authorize!
    super

    raise Doorkeeper::Errors::InvalidToken, "User (#{current_user.email}) is archived." if current_user.archived?
  end

  def current_user
    @current_user ||= User.find(doorkeeper_token.resource_owner_id)
  end
end
