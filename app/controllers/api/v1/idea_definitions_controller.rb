# frozen_string_literal: true

module Api::V1
  class IdeaDefinitionsController < ::ApiController
    def create
      permitted_params = params.require(:idea_definition).permit(
        :idea_template_id, :name, :description, :timezone, :anonymous, :report_shareable, timer: {},
        channels: %i[category ref_team_id channel_id name],
        invited_participants: %i[category email name ref_team_id ref_user_id], questions: %i[content]
      )
      idea_definition = IdeaDefinitionService.create(permitted_params, current_user)

      render_json(
        idea_definition: idea_definition.as_json(
          only: %i[id workspace_id user_id idea_template_id name description status repeatable timezone timer channels
                   invited_participants report_shareable anonymous archived_at],
          include: { questions: { only: %i[id ordinal content required] } },
        ),
      )
    end

    def destroy
      IdeaDefinitionService.archive(current_user, params.require(:id))

      render_json
    end

    def index
      singleton_definitions = IdeaDefinitionService.scoped_resources(current_user).includes(:user)

      definitions = singleton_definitions.map do |definition|
        running_status = definition.singleton_idea&.running_status || Idea::RunningStatus::NOT_STARTED

        definition.slice(:id, :user_id, :name, :invited_participants)
          .merge(
            start_time: definition.timer[:start_time],
            release_time: definition.timer[:release_time],
            running_status: running_status,
            idea_id: definition.singleton_idea&.id,
          )
      end
      users = singleton_definitions.map(&:user).uniq.map { |user| UserService.build_json(user) }

      render_json idea_definitions: definitions, users: users
    end

    def show
      idea_definition = IdeaDefinitionService.scoped_resources(current_user).find(params.require(:id))
      render_json(
        idea_definition: idea_definition.as_json(
          only: %i[id workspace_id user_id idea_template_id name description status repeatable timezone timer channels
                   invited_participants report_shareable anonymous archived_at],
          include: { questions: { only: %i[id ordinal content required] } },
        ),
      )
    end

    def update
      permitted_params = params.require(:idea_definition).permit(
        :idea_template_id, :name, :description, :timezone, :report_shareable, :anonymous, timer: {},
        channels: %i[category ref_team_id channel_id name],
        invited_participants: %i[category ref_team_id ref_user_id], questions: %i[required content]
      )
      IdeaDefinitionService.update(params.require(:id), permitted_params, current_user)

      render_json
    end
  end
end
