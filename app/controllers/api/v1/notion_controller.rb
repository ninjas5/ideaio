# frozen_string_literal: true

module Api::V1
  class NotionController < ::ApiController
    def check
      connected = NotionService.connection_exists(current_user.id)
      render_json result: connected, url: notion_oauth_url
    end

    def connect
      if NotionService.connection_exists(current_user.id)
        render_json code: 1, message: 'You already connect with Notion, please reconnect'
      else
        # render_json rediret: url
        redirect_to notion_oauth_url
      end
    end

    def disconnect
      if NotionService.disconnect(current_user)
        render_json url: notion_oauth_url
      else
        render_json code: 1, message: 'Notion disconnection fails, please try again later'
      end
    end

    def export
      user_id = current_user.id
      page_id = params.require(:page_id)
      idea_id = params.require(:idea_id)

      idea = IdeaReportService.scoped_ideas(current_user).includes(
        idea_definition: { user: :slack_user },
        questions: { question_answers: { idea_participant: { user: :slack_user } } },
      ).find(idea_id)

      if NotionService.send_to_notion(user_id, page_id, idea)
        render_json
      else
        render_json code: 1, message: 'Data export to Notion fails'
      end
    end

    def callback
      temp_code = params.require(:code)
      state = params.require(:state)
      user_id = Base64.strict_decode64(state)
      current_user = User.find_by(id: user_id)
      conn = NotionService.oauth(temp_code, current_user)

      if conn
        render_json
      else
        render_json code: 1, message: 'Notion Authentication Error'
      end
    end

    def dblist
      dblist = NotionService.db_list(current_user)
      if dblist.nil?
        render_json code: 1, message: 'Unable to get page list from Notion'
      else
        render_json db_list: dblist
      end
    end

    private

    def notion_oauth_url
      client_id = NotionConst::IdeaioBot::CLIENT_ID
      redirect_uri = CommonConst::IDEA_HOSTS[Rails.env] + NotionConst::IdeaioBot::REDIRECT_URI
      encrypted_userid = Base64.strict_encode64(current_user.id.to_s)

      NotionConst::IdeaioBot::AUTH_URI + \
        "?owner=user&client_id=#{client_id}&redirect_uri=#{redirect_uri}" \
        "&response_type=code&state=#{encrypted_userid}"
    end
  end
end
