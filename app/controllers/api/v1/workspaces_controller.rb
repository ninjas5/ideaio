# frozen_string_literal: true

module Api::V1
  class WorkspacesController < ::ApiController
    def index
      # TODO: 暂不支持切换workspace
      render_json workspaces: [current_user.workspace]
    end
  end
end
