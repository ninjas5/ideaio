# frozen_string_literal: true

module Api::V1
  class QuestionAnswersController < ::ApiController
    def vote
      # TODO
      answer = QuestionAnswer.find(params.require(:id))
      if answer.vote_user_ids.include?(current_user.id)
        answer.vote_user_ids.delete(current_user.id)
      else
        answer.vote_user_ids << current_user.id
      end
      answer.save!

      render_json question_answer: answer.slice(:id, :vote_user_ids)
    end
  end
end
