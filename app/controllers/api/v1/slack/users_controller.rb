# frozen_string_literal: true

module Api::V1::Slack
  class UsersController < ::ApiController
    def index
      render_json users: SlackService.list_users(current_user.workspace, params.require(:channels).split(','))
    end

    MOCKED_USERS = [
      {
        ref_team_id: 'T024BE912',
        ref_user_id: 'C024BE91K',
        email: 'sky@test.com',
        profile: { email: 'sky@test.com' },
      },
      {
        ref_team_id: 'T024BE911',
        ref_user_id: 'C024BE91L',
        email: 'email@test.com',
        profile: { email: 'emma@test.com' },
      },
      {
        ref_team_id: 'T024BE911',
        ref_user_id: 'C024BE91M',
        email: 'gy@test.com',
        profile: { email: 'gy@test.com' },
      },
    ].freeze
  end
end
