# frozen_string_literal: true

module Api::V1::Slack
  class ChannelsController < ::ApiController
    def index
      rst = SlackService.list_channels(current_user.workspace)

      if rst.ok
        render_json channels: rst[:channels]
      else
        render_json rst
      end
    end
  end
end
