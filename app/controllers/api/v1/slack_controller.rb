# frozen_string_literal: true

module Api::V1
  class SlackController < ::ApiController
    skip_before_action :doorkeeper_authorize!, only: %i[login oauth_authorize]

    def login; end

    def logout
      UserService.logout(current_user)

      render_json
    end

    def oauth_authorize
      if params[:error].present?
        logger.error { "Slack oauth authorize error, params: #{params}" }
        return request_http_token_authentication
      end

      access_token = SlackService.oauth_access(params.require(:code))
      if access_token
        render_json(**access_token.slice(:token, :expires_at))
      else
        render_json code: 1, message: 'Slack OAuth accessor error'
      end
    end
  end
end
