# frozen_string_literal: true

module Api::V1
  class IdeaReportsController < ::ApiController
    JSON_KEY_IDEA_PARTICIPANT = {
      only: %i[id user_id],
      include: {
        question_answers: {
          only: %i[id idea_question_id content tags vote_user_ids created_at updated_at],
          include: {
            comments: {
              only: %i[id content user_id created_at],
            },
          },
        },
      },
    }.freeze

    def index
      ideas = IdeaReportService.scoped_ideas(current_user).includes(idea_definition: { user: :slack_user })

      idea_reports = ideas.as_json(
        only: %i[id name description timezone start_time release_time report_shareable],
        include: { idea_definition: { only: [:user_id] } },
      )
      idea_reports.each { |item| item['user_id'] = item['idea_definition'].delete('user_id') }
      # user as a creator report
      users = ideas.map { |idea| idea.idea_definition.user }.uniq.map { |user| UserService.build_json(user) }

      render_json idea_reports: idea_reports, users: users
    end

    def show
      idea = IdeaReportService.scoped_ideas(current_user).includes(
        idea_definition: { user: :slack_user },
        questions: { question_answers: { idea_participant: { user: :slack_user } } },
      ).find(params.require(:id))

      idea_report = idea.as_json(only: %i[id name description start_time release_time]).with_indifferent_access
      idea_report[:user_id] = idea.idea_definition.user_id
      idea_report[:start_time] = IdeaTimeUtil.format_to_str(idea.start_time, idea.timezone)
      idea_report[:release_time] = IdeaTimeUtil.format_to_str(idea.release_time, idea.timezone)
      idea_report[:questions] = idea.questions.map do |question|
        question.as_json(only: %i[id ordinal content])
          .merge(
          answers: question.question_answers.map do |answer|
            answer.as_json(only: %i[id content tags vote_user_ids]).merge(
              user_id: answer.idea_participant.user_id,
              created_at: IdeaTimeUtil.format_to_str(answer.created_at, idea.timezone),
              updated_at: IdeaTimeUtil.format_to_str(answer.updated_at, idea.timezone),
              comments: answer.comments.map do |comment|
                comment.as_json(only: %i[id content user_id]).merge(
                  created_at: IdeaTimeUtil.format_to_str(comment.created_at, idea.timezone),
                )
              end,
            )
          end,
        )
      end

      temp_users = [idea.idea_definition.user]
      if idea.report_shareable || idea.idea_definition.user.eql?(current_user)
        temp_users.concat(idea.idea_participants.map(&:user))
      else
        idea_report[:questions].each do |question|
          question[:answers].select! { |item| item[:user_id].eql?(current_user.id) }
        end
      end
      users = temp_users.uniq.map { |user| UserService.build_json(user) }
      if idea.anonymous
        render_json idea_report: idea_report
      else
        render_json idea_report: idea_report, users: users
      end
    end
  end
end
