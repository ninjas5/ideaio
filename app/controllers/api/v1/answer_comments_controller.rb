# frozen_string_literal: true

module Api::V1
  class AnswerCommentsController < ::ApiController
    def create
      comment = QuestionAnswer.find(params.require(:question_answer_id))
        .comments.create!(workspace: current_user.workspace, user: current_user, content: params.require(:content))

      render_json id: comment.id
    end

    def destroy
      QuestionAnswer.find(params.require(:question_answer_id))
        .comments
        .find_by!(id: params.require(:id), user: current_user)
        .destroy!

      render_json
    end
  end
end
