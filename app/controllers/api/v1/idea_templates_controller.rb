# frozen_string_literal: true

module Api::V1
  class IdeaTemplatesController < ::ApiController
    def index
      templates = IdeaTemplateService.filter_by_user(current_user)
      json = templates.as_json(
        only: %i[id category name description workspace_id user_id],
      )
      render_json idea_templates: json
    end

    def show
      template = IdeaTemplateService.filter_by_user(current_user).find_by(id: params.require(:id))
      questions = IdeaTemplateService.get_questions_by_template_id(params.require(:id)).as_json(
        only: %i[id ordinal content idea_template_id],
      )
      render_json name: template.name, description: template.description, questions: questions
    end
  end
end
