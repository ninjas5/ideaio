# frozen_string_literal: true

module Api::V1
  class SettingsController < ::ApiController
    skip_before_action :doorkeeper_authorize!

    def timezones
      render_json timezones: CommonConst::TIMEZONES
    end
  end
end
