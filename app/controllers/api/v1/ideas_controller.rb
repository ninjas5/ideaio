# frozen_string_literal: true

module Api::V1
  class IdeasController < ::ApiController
    def index
      ideas = IdeaService.scoped_ideas(current_user).includes(idea_definition: { user: { slack_user: :slack_team } })
      result = ideas.map do |idea|
        idea.as_json(only: %i[id name start_time release_time running_status])
          .merge(user: UserService.build_json(idea.idea_definition.user))
      end

      render_json ideas: result
    end

    def show
      idea = IdeaService.scoped_ideas(current_user).find(params.require(:id))

      result = idea.as_json(
        only: %i[id name description start_time release_time timezone anonymous
                 invited_participants report_shareable status running_status],
        include: { questions: { only: %i[id ordinal content required] } },
      )
      result['user_id'] = idea.idea_definition.user_id

      idea_participant = idea.idea_participants.find_by(user: current_user)
      if idea_participant
        result['answers'] = idea_participant.question_answers
          .as_json(only: %i[id idea_question_id content tags vote_user_ids created_at updated_at])
      end

      render_json idea: result, users: [UserService.build_json(idea.idea_definition.user)]
    end

    # 只需要回答单个问题
    def answer
      permitted_params = params.permit(question_answer: [:idea_question_id, :content, { tags: [] }])
      IdeaService.answer(params.require(:id), permitted_params.require(:question_answer), current_user)

      render_json
    end
  end
end
