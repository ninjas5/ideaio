# frozen_string_literal: true

module Api::V1
  class UsersController < ::ApiController
    skip_before_action :doorkeeper_authorize!, only: %i[access_tokens] unless Rails.env.production?

    def profile
      render_json user: UserService.build_json(current_user)
    end

    def archive
      UserService.logout(current_user)

      current_user.archive!

      render_json
    end

    # TODO: 只是为了方便在开发和测试暴露此接口
    def access_tokens
      render_json users: User.includes(:active_access_tokens, :workspace).as_json(
        only: %i[id email],
        include: {
          workspace: { only: %i[id name] },
          active_access_tokens: { only: %i[id token expires_in revoked_at created_at], methods: %i[expires_at] },
        },
      )
    end
  end
end
