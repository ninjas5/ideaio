# frozen_string_literal: true

class IdeaTimeUtil
  include Singleton
  include CustomLogger

  class << self
    DELEGATED_METHODS = %i[format_to_str].freeze
    delegate(*DELEGATED_METHODS, to: :instance)
  end

  def format_to_str(time, timezone = nil)
    zone = timezone ? Time.find_zone!(timezone) : Time.utc
    zone.at(time).strftime('%m-%d-%Y %I:%M %p')
  end
end
