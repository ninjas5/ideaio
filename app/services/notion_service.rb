# frozen_string_literal: true

require 'faraday'
require 'faraday/net_http'

class NotionService
  include Singleton
  include CustomLogger

  class << self
    DELEGATED_METHODS = %i[connection_exists oauth connect disconnect db_list send_to_notion
                           post_idea_report].freeze
    delegate(*DELEGATED_METHODS, to: :instance)
  end

  def oauth(temp_code, current_user)
    params = {
      grant_type: 'authorization_code',
      code: temp_code,
      redirect_uri: CommonConst::IDEAIO_BASE_URL + NotionConst::IdeaioBot::REDIRECT_URI,
    }.to_json

    conn = Faraday.new(
      url: NotionConst::IdeaioBot::NOTION_HOST,
      headers: { 'Content-Type' => 'application/json', 'Authorization' => "Basic #{credential}" },
    )

    response = conn.post('/v1/oauth/token', params)
    return false unless response.status == 200

    conn = nil
    result = JSON.parse(response.body)

    begin
      # bot_id is the PK for each authenticated notion connection
      conn = NotionConnection.transaction do
        NotionConnection.create!(
          access_token: result['access_token'],
          token_type: result['token_type'],
          bot_id: result['bot_id'],
          workspace_name: result['workspace_name'],
          workspace_id: result['workspace_id'],
          workspace_icon: result['workspace_icon'],
          owner: result['owner'],
          user: current_user,
        )
      end
    rescue StandardError => e
      raise e
    end
    !conn.nil?
  end

  def disconnect(current_user)
    NotionConnection.where(user_id: current_user.id).destroy_all
    !NotionConnection.exists?(current_user.id)
  end

  # only allow user to select pages
  # we don't want to deal with existing db structures
  def db_list(user)
    connection = user.notion_connection
    return nil if connection.nil?

    results = connection.client.search(filter: { property: 'object', value: 'page' }).results
    return nil if results.blank?

    dblist = []
    results.each do |page|
      if page.properties.try(:title)
        dblist << { id: page.id, name: page.properties.title.title[0].plain_text }
      else
        Rails.logger.debug { "#{page.url} contains no title" }
      end
    end
    dblist
  end

  def report_content(page_id, idea)
    children = []
    result = {
      parent: { page_id: page_id },
      properties: {
        title: [
          {
            type: 'text',
            text: {
              content: "#{idea.idea_definition.name} #{IdeaTimeUtil.format_to_str(idea.release_time, idea.timezone)}",
            },
          },
        ],
      },
    }

    idea.questions.each do |question|
      children << {
        object: 'block',
        type: 'heading_3',
        heading_3: {
          rich_text: [
            {
              type: 'text',
              text: {
                content: question.content,
              },
            },
          ],
        },
      }
      question.question_answers.each do |answer|
        children << {
          object: 'block',
          type: 'paragraph',
          paragraph: {
            rich_text: [
              {
                type: 'text',
                text: {
                  content: answer.content,
                },
              },
            ],
          },
        }
      end
    end

    result.merge!(children: children)
  end

  def send_to_notion(user_id, page_id, idea)
    access_token = NotionConnection.where(user_id: user_id).first.access_token
    # all the keys in report content are required
    params = report_content(page_id, idea).to_json
    conn = Faraday.new(
      url: NotionConst::IdeaioBot::NOTION_HOST,
      headers: {
        'Content-Type' => 'application/json',
        'Authorization' => "Bearer #{access_token}",
        'Notion-Version' => '2022-02-22',
        'Accept' => 'application/json',
      },
    ) do |f|
      f.request :json # encode req bodies as JSON and automatically set the Content-Type header
      f.request :retry # retry transient failures
      f.response :json # decode response bodies as JSON
    end
    response = conn.post('/v1/pages', params)
    response.status == 200
  end

  def credential
    Base64.strict_encode64("#{NotionConst::IdeaioBot::CLIENT_ID}:#{NotionConst::IdeaioBot::CLIENT_SECRET}")
  end

  def connection_exists(user_id)
    NotionConnection.where(user_id: user_id).present?
  end

  def post_idea_report(idea)
    logger.info { "Post idea report to notion: #{idea}" }
    # TODO: @emma
  rescue StandardError => e
    logger.error(e)
  end
end
