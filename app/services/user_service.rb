# frozen_string_literal: true

class UserService
  include Singleton
  include CustomLogger

  class << self
    DELEGATED_METHODS = %i[build_json renew_token logout].freeze
    delegate(*DELEGATED_METHODS, to: :instance)
  end

  def build_json(user)
    result = user.as_json(only: %(id), include: { slack_user: { only: %i[ref_user_id email profile] } })
    result['slack_user']['ref_team_id'] = user.slack_user.slack_team.ref_team_id
    result
  end

  def renew_token(user)
    ideaio = OauthApplications.ideaio

    user.transaction do
      logout(user)

      user.access_grants.create!(
        application: ideaio,
        redirect_uri: ideaio.redirect_uri,
        scopes: ideaio.scopes,
        expires_in: CommonConst::ACCESS_GRANT_EXPIRES_IN,
      )

      user.access_tokens.create!(
        application: ideaio,
        scopes: ideaio.scopes,
        expires_in: CommonConst::ACCESS_TOKEN_EXPIRES_IN,
        use_refresh_token: true,
      )
    end
  end

  def logout(user)
    ideaio = OauthApplications.ideaio
    user.transaction do
      user.access_grants.where(application: ideaio, revoked_at: nil).each { |item| item.expired? || item.revoke }
      user.access_tokens.where(application: ideaio, revoked_at: nil).each { |item| item.expired? || item.revoke }
    end
  end
end
