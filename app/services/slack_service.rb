# frozen_string_literal: true

class SlackService
  include Singleton
  include CustomLogger

  class << self
    DELEGATED_METHODS = %i[oauth_access filter_by_user list_channels list_users slack_client].freeze
    delegate(*DELEGATED_METHODS, to: :instance)
  end

  def filter_by_user(user)
    IdeaTemplate.where(workspace: nil, user: nil)
      .union_all(IdeaTemplate.where(workspace: user.workspace, user: nil))
      .union_all(IdeaTemplate.where(user: user))
  end

  def oauth_access(code)
    params = SlackConst::IdeaioBot::CONFIG.slice('client_id', 'client_secret').merge(
      code: code,
      redirect_uri: CommonConst::SLACK_AUTH_URL,
      grant_type: SlackConst::GrantType::AUTHORIZATION_CODE,
    )

    result = slack_client.oauth_v2_access(params)
    logger.info { "Call oauth_v2_access: #{result.to_json}" }

    return nil unless result.ok

    slack_team = SlackTeam.find_by(ref_team_id: result.team.id)
    SlackUser.transaction do
      if slack_team.nil?
        workspace = Workspace.create!(category: Workspace::Category::SLACK, name: result.team.name)
        slack_team = SlackTeam.new(workspace: workspace, ref_team_id: result.team.id)
      end
      slack_team.update!(
        ref_team_name: result.team.name,
        scope: result.scope,
        access_token: result.access_token,
        refresh_token: result.refresh_token,
        expires_in: result.expires_in,
        expires_at: Time.zone.now + result.expires_in,
      )

      slack_user = slack_team.slack_users.find_by(ref_user_id: result.authed_user.id)
      if slack_user.nil?
        user = slack_team.workspace.users.create!(email: '', roles: [])
        slack_user = slack_team.slack_users.new(
          workspace: slack_team.workspace,
          user: user,
          email: '',
          ref_user_id: result.authed_user.id,
        )
      end

      update_user_profile(slack_user)
      UserService.renew_token(slack_user.user)
    end
  rescue StandardError => e
    logger.error(e)
    nil
  end

  def list_channels(workspace)
    try = 0

    begin
      workspace.slack_client.conversations_list
    rescue Slack::Web::Api::Errors::TokenExpired
      workspace.refresh_access_token

      try += 1
      try <= SlackConst::DEFAULT_RETRY_TIMES ? retry : raise
    end
  end

  def list_users(workspace, channel_ids)
    try = 0

    begin
      member_ids = channel_ids.reduce([]) do |_tmp, channel_id|
        workspace.slack_client.conversations_members(channel: channel_id).members
      end.uniq

      workspace.slack_client.users_list.members
        .select { |member| member_ids.include?(member.id) }
        .map do |member|
          {
            ref_team_id: member.team_id,
            ref_user_id: member.id,
            email: member.profile.email,
            profile: member.profile,
          }
        end
    rescue Slack::Web::Api::Errors::TokenExpired
      workspace.refresh_access_token

      try += 1
      try <= SlackConst::DEFAULT_RETRY_TIMES ? retry : raise
    end
  end

  def slack_client
    @slack_client ||= Slack::Web::Client.new
  end

  private

  def update_user_profile(slack_user)
    try = 0

    begin
      rst = slack_user.slack_team.slack_client.users_profile_get(user: slack_user.ref_user_id)
      logger.info("Get user #{slack_user.ref_user_id} profile finished. #{rst}")
    rescue Slack::Web::Api::Errors::TokenExpired
      slack_user.slack_team.refresh_access_token

      try += 1
      try <= SlackConst::DEFAULT_RETRY_TIMES ? retry : raise
    end

    if rst.ok?
      slack_user.update!(email: rst.profile.email, profile: rst.profile)
      slack_user.user.update!(email: rst.profile.email, archived_at: nil)
    end
  rescue StandardError => e
    logger.error(e)
  end
end
