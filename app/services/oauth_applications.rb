# frozen_string_literal: true

class OauthApplications
  include Singleton
  include CustomLogger

  class << self
    DELEGATED_METHODS = %i[ideaio].freeze
    delegate(*DELEGATED_METHODS, to: :instance)
  end

  def ideaio
    @ideaio ||= Doorkeeper::Application.find_by!(name: CommonConst::OAuthApplication::IDEAIO)
  end
end
