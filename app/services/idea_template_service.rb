# frozen_string_literal: true

class IdeaTemplateService
  class << self
    # this may be needed in future when a user wants to create his/her own templates
    def filter_by_user(user)
      IdeaTemplate.enabled.where(workspace: nil, user: nil)
        .union_all(IdeaTemplate.enabled.where(workspace: user.workspace, user: nil))
        .union_all(IdeaTemplate.enabled.where(user: user))
    end

    def get_questions_by_template_id(template_id)
      IdeaTemplateQuestion.where(idea_template_id: template_id)
    end
  end
end
