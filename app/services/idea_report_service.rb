# frozen_string_literal: true

class IdeaReportService
  class << self
    def scoped_ideas(user)
      all_ideas = Idea.where(workspace: user.workspace, running_status: Idea::RunningStatus::RELEASED)

      created_ideas = all_ideas.joins(:idea_definition).where(idea_definition: { user: user })

      joined_ideas = all_ideas.report_shareable.filter_participant_by_user(user)

      created_ideas.union_all(joined_ideas).distinct
    end
  end
end
