# frozen_string_literal: true

module CustomLogger
  extend ActiveSupport::Concern

  included do
    def logger
      self.class.logger
    end
  end

  module ClassMethods
    def logger
      Rails.logger
    end
  end
end
