# frozen_string_literal: true

class IdeaService
  class << self
    def scoped_ideas(user)
      all_ideas = Idea.where(workspace: user.workspace)

      created_ideas = all_ideas.joins(:idea_definition).where(idea_definition: { user: user })
      joined_ideas = all_ideas.filter_participant_by_user(user)

      created_ideas.union_all(joined_ideas).distinct
    end

    # IdeaService.answer(params.require(:id), permitted_params, current_user)
    def answer(id, params, user)
      idea = Idea.where(workspace: user.workspace, running_status: Idea::RunningStatus::STARTED)
        .filter_participant_by_user(user).find(id)
      idea_participant = idea.idea_participants.find_by(user: user)

      idea.transaction do
        idea_participant ||= idea.idea_participants.create!(workspace: user.workspace, user: user)

        question = idea.questions.find(params[:idea_question_id])
        ans = idea_participant.question_answers.find_by(idea_question: question) ||
              idea_participant.question_answers.new(workspace: user.workspace, idea_question: question)

        ans.update!(params.slice(:content, :tags))
      end
    end
  end
end
