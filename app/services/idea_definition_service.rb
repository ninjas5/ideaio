# frozen_string_literal: true

class IdeaDefinitionService
  class << self
    def create(params, user)
      questions = params.require(:questions)
        .each_with_index
        .map { |item, index| IdeaQuestionDefinition.new(item.merge(ordinal: index)) }
        .select { |item| item.content.present? }

      create_params = default_options.merge(params.except(:questions)).merge(workspace: user.workspace,
        questions: questions)
      user.idea_definitions.create!(create_params)
    end

    def update(id, params, user)
      resource = IdeaDefinition.not_archived.singleton.left_outer_joins(:ideas)
        .find_by!(ideas: { id: nil }, user: user, id: id)

      exist_question_hs = resource.questions.index_by(&:ordinal)

      resource.transaction do
        params.delete(:questions).each_with_index do |question_hs, index|
          question = exist_question_hs.delete(index) || resource.questions.new(ordinal: index)
          question.update!(question_hs)
        end
        exist_question_hs.each_value(&:destroy!)

        resource.update!(params)
      end

      resource
    end

    def scoped_resources(user)
      valid_definitions = IdeaDefinition.not_archived.singleton

      # Created(NOT_STARTED + STARTED)
      not_started_resources = valid_definitions.left_outer_joins(:ideas).where(ideas: { id: nil }).where(user: user)
      started_resources = valid_definitions.joins(:ideas)
        .where(ideas: { running_status: Idea::RunningStatus::STARTED })
        .where(user: user)

      # Joined(STARTED)
      joined_resources = valid_definitions.joins(:ideas)
        .where(ideas: { running_status: Idea::RunningStatus::STARTED })
        .where(
          'ideas.invited_participants @> ?',
          [{
            category: user.workspace.category,
            ref_team_id: user.slack_user.slack_team.ref_team_id,
            ref_user_id: user.slack_user.ref_user_id,
          }].to_json,
        )

      not_started_resources.union_all(started_resources).union_all(joined_resources).distinct
    end

    def archive(user, id)
      resource = IdeaDefinition.not_archived.singleton.left_outer_joins(:ideas)
        .where(ideas: { id: nil })
        .where(user: user)
        .find(id)

      resource.archive!
    end

    private

    def default_options
      @default_options ||= {
        status: IdeaDefinition::Status::DISABLED,
        repeatable: false,
      }
    end
  end
end
