# frozen_string_literal: true

class SlackMessageService
  include Singleton
  include CustomLogger

  class << self
    DELEGATED_METHODS = %i[post_idea_invites post_idea_released].freeze
    delegate(*DELEGATED_METHODS, to: :instance)
  end

  def post_idea_invites(idea)
    idea.invited_participants.each do |participant|
      try = 0

      begin
        idea.workspace.slack_client.chat_postMessage(
          channel: participant['ref_user_id'],
          text: build_idea_invite_message(idea),
        )
      rescue Slack::Web::Api::Errors::TokenExpired
        idea.workspace.refresh_access_token

        try += 1
        try <= SlackConst::DEFAULT_RETRY_TIMES ? retry : raise
      end
    end
  end

  def post_idea_released(idea)
    try = 0

    begin
      idea.workspace.slack_client.chat_postMessage(
        channel: idea.idea_definition.user.slack_user.ref_user_id,
        text: build_idea_released_message(idea),
      )
    rescue Slack::Web::Api::Errors::TokenExpired
      idea.workspace.refresh_access_token

      try += 1
      try <= SlackConst::DEFAULT_RETRY_TIMES ? retry : raise
    end
  end

  private

  def build_idea_invite_message(idea)
    url = "#{CommonConst::IDEAIO_BASE_URL}/dashboard/answer/#{idea.id}"
    ref_user_id = idea.idea_definition.user.slack_user.ref_user_id
    msg = ":wave: <@#{ref_user_id}> invited you to participate in <#{url}|this ideation>. "
    msg += "The due time is `#{IdeaTimeUtil.format_to_str(idea.release_time, idea.timezone)}`. "
    "#{msg}Good thoughts take time to bake. :thinking_face: Enjoy!"
  end

  def build_idea_released_message(idea)
    url = "#{CommonConst::IDEAIO_BASE_URL}/reports/detail/#{idea.id}"
    "Your ideation report is generated. Please visit <#{url}|this link> for details :high_brightness:"
  end
end
