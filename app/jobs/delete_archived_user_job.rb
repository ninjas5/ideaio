# frozen_string_literal: true

class DeleteArchivedUserJob < ApplicationJob
  MOCKED_IMAGE = 'https://app.ideaio.xyz/img/logo.f151b856.png'

  def perform(user_id)
    user = User.archived.find_by(id: user_id)
    return if user.nil?

    return anonymous_user(user) if user.archived_at <= CommonConst::ACCOUNT_DELETION_BUFFER.ago

    # 重试
    self.class.set(wait_until: user.archived_at - CommonConst::ACCOUNT_DELETION_BUFFER).perform_later(user_id)
  end

  private

  def anonymous_user(user)
    return if user.email.match?(/\Aanonymous-.+@ideaio.xyz\z/)

    uuid = SecureRandom.uuid
    mocked_email = "anonymous-#{uuid}@ideaio.xyz"

    user.transaction do
      # disconnect notion
      user.notion_connection&.destroy!

      # mock slack user
      user.slack_user.update!(
        ref_user_id: "#{user.slack_user.ref_user_id}-#{uuid}",
        email: mocked_email,
        access_token: "access_token-#{uuid}",
        refresh_token: "refresh_token-#{uuid}",
        profile: user.slack_user.profile.merge(
          email: mocked_email,
          phone: '',
          skype: '',
          title: '',
          fields: nil,
          image_24: MOCKED_IMAGE,
          image_32: MOCKED_IMAGE,
          image_48: MOCKED_IMAGE,
          image_72: MOCKED_IMAGE,
          image_192: MOCKED_IMAGE,
          image_512: MOCKED_IMAGE,
          last_name: 'Anonymous',
          real_name: 'Anonymous',
          first_name: 'Anonymous',
          image_1024: MOCKED_IMAGE,
          image_original: MOCKED_IMAGE,
          avatar_hash: mocked_email.hash.to_s,
          status_text: '',
          display_name: '',
          status_emoji: '',
          is_custom_image: true,
          status_expiration: 0,
          real_name_normalized: 'Anonymous',
          status_text_canonical: '',
          display_name_normalized: '',
          status_emoji_display_info: [],
        ),
      )

      # mock user
      user.update!(email: mocked_email)
    end
  end
end
