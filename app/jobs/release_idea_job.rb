# frozen_string_literal: true

class ReleaseIdeaJob < ApplicationJob
  def perform(idea_id)
    idea = Idea.find_by(running_status: Idea::RunningStatus::STARTED, id: idea_id)
    return if idea.nil?

    # 重试
    return ReleaseIdeaJob.set(wait_until: idea.release_time).perform_later(idea_id) if Time.now.utc < idea.release_time

    release_idea(idea)
  end

  private

  def release_idea(idea)
    idea.transaction do
      idea.update!(running_status: Idea::RunningStatus::RELEASED)
    end

    SlackMessageService.post_idea_released(idea)
  end
end
