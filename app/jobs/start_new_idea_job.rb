# frozen_string_literal: true

class StartNewIdeaJob < ApplicationJob
  def perform(idea_definition_id)
    definition = IdeaDefinition.not_archived.find_by(id: idea_definition_id)
    return if definition.repeatable

    expected_time = Time.find_zone(definition.timezone).parse definition.timer['start_time']
    now_time = Time.now.utc

    # 重试
    return StartNewIdeaJob.set(wait_until: expected_time).perform_later(idea_definition_id) if now_time < expected_time

    if definition.user.archived_at
      return logger.info { "IdeaDefinition(\##{definition.id})'s creator (User\##{definition.user_id}) is archived" }
    end

    publish_idea(definition)
  end

  private

  def publish_idea(definition)
    idea_params = definition.slice(:workspace_id, :name, :description, :timezone, :invited_participants,
      :anonymous, :report_shareable)
    idea_params[:status] = Idea::Status::ENABLED
    idea_params[:running_status] = Idea::RunningStatus::STARTED
    idea_params[:start_time] = Time.find_zone(definition.timezone).parse definition.timer['start_time']
    idea_params[:release_time] = Time.find_zone(definition.timezone).parse definition.timer['release_time']

    definition.transaction do
      idea = definition.ideas.create!(idea_params)
      raise ActiveRecord::Rollback if definition.ideas.exists?(['id < ?', idea.id])

      definition.questions.map { |question| idea.questions.create!(question.slice(:ordinal, :content, :required)) }

      SlackMessageService.post_idea_invites(idea)
    end
  end
end
