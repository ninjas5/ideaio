# frozen_string_literal: true

class ApplicationJob < ActiveJob::Base
  class JobNeedRetryError < StandardError; end

  # Automatically retry jobs that encountered a deadlock
  retry_on ActiveRecord::Deadlocked

  # Retry (seconds): 10, 40, 90, 160, 250, 360
  retry_on JobNeedRetryError, attempts: 6, wait: ->(executions) { 10 * (executions**2) }

  # Most jobs are safe to ignore if the underlying records are no longer available
  discard_on ActiveJob::DeserializationError
end
