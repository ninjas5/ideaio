# frozen_string_literal: true

class SlackTeam < ApplicationRecord
  belongs_to :workspace

  has_many :slack_users

  def slack_client
    @slack_client ||= Slack::Web::Client.new(token: access_token)
  end

  def cleanup_slack_client
    @slack_client = nil
  end

  def refresh_access_token
    cleanup_slack_client

    params = SlackConst::IdeaioBot::CONFIG.slice('client_id', 'client_secret')
      .merge(grant_type: SlackConst::GrantType::REFRESH_TOKEN, refresh_token: refresh_token)

    result = SlackService.slack_client.oauth_v2_access(params)
    logger.info(result)
    return false unless result.ok?

    update!(
      ref_team_name: result.team.name,
      scope: result.scope,
      access_token: result.access_token,
      refresh_token: result.refresh_token,
      expires_in: result.expires_in,
      expires_at: Time.zone.now + result.expires_in,
    )

    true
  end
end
