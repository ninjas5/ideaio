# frozen_string_literal: true

class IdeaQuestionDefinition < ApplicationRecord
  belongs_to :idea_definition

  validates :ordinal, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :content, presence: true
end
