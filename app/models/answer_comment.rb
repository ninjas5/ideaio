# frozen_string_literal: true

class AnswerComment < ApplicationRecord
  belongs_to :workspace
  belongs_to :user
  belongs_to :question_answer
end
