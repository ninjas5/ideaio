# frozen_string_literal: true

class QuestionAnswer < ApplicationRecord
  belongs_to :workspace
  belongs_to :idea_participant
  belongs_to :idea_question

  has_many :comments, class_name: 'AnswerComment'
end
