# frozen_string_literal: true

module Archiveable
  extend ActiveSupport::Concern

  included do
    scope :not_archived, -> { where archived_at: nil }
    scope :archived, -> { where.not archived_at: nil }

    def archived?
      !not_archived?
    end

    def not_archived?
      archived_at.nil?
    end

    def archive!(force: false)
      update!(archived_at: Time.zone.now) if force || not_archived?
    end
  end
end
