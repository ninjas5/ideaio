# frozen_string_literal: true

class IdeaTemplateQuestion < ApplicationRecord
  belongs_to :idea_template
end
