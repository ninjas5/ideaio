# frozen_string_literal: true

class NotionConnection < ApplicationRecord
  belongs_to :user

  def client
    Notion::Client.new(token: access_token)
  end
end
