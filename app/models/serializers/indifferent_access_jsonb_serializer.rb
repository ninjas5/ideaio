# frozen_string_literal: true

require 'active_support/core_ext/hash'

module Serializers
  class IndifferentAccessJsonbSerializer
    def self.dump(obj)
      obj
    end

    def self.load(source)
      case source
      when Hash
        source.with_indifferent_access
      when Enumerable
        # Recursively convert all enumerated children to indifferent access
        source.map { |nested_source| load(nested_source) }
      else
        source
      end
    end
  end
end
