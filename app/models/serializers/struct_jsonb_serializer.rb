# frozen_string_literal: true

module Serializers
  class StructJsonbSerializer
    def initialize(struct)
      @struct = struct
    end

    def self.[](struct)
      StructJsonbSerializer.new(struct)
    end

    def dump(obj)
      unless obj.is_a?(@struct)
        raise ActiveRecord::SerializationTypeMismatch,
          "Can't dump: was supposed to be a #{@struct}, but was a #{obj.class}. -- #{obj.inspect}"
      end

      obj.serialize
    end

    def load(source)
      source ? @struct.from_hash(source) : nil
    end
  end
end
