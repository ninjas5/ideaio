# frozen_string_literal: true

class IdeaParticipant < ApplicationRecord
  belongs_to :workspace
  belongs_to :user
  belongs_to :idea

  has_many :question_answers
end
