# frozen_string_literal: true

class IdeaQuestion < ApplicationRecord
  belongs_to :idea

  has_many :question_answers
end
