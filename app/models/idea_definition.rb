# frozen_string_literal: true

class IdeaDefinition < ApplicationRecord
  include Archiveable

  START_IDEA_FREEZE_MINUTES = 0 # 从 idea_definition 开始之前的 xx 分钟之后，不允许在修改 idea_definition
  MIN_IDEA_DURATION_MINUTES = 1 # 每个 idea 最少需要持续 xx 分钟

  belongs_to :workspace
  belongs_to :user
  belongs_to :idea_template, optional: true

  has_many :questions, class_name: 'IdeaQuestionDefinition'
  has_many :ideas

  serialize :timer, Serializers::IndifferentAccessJsonbSerializer
  serialize :invited_participants, Serializers::IndifferentAccessJsonbSerializer

  module Status
    extend ConstantValue

    ENABLED = 'enabled'
    DISABLED = 'disabled'
  end

  scope :singleton, -> { where repeatable: false }
  scope :filter_participant, lambda { |category: nil, ref_team_id: nil, ref_user_id: nil|
    condition = {}
    condition[:category] = category if category.present?
    condition[:ref_team_id] = ref_team_id if ref_team_id.present?
    condition[:ref_user_id] = ref_user_id if ref_user_id.present?
    where 'idea_definitions.invited_participants @> ?', [condition].to_json
  }

  after_create :setup_new_idea_job

  validates :name, presence: true
  validates :status, inclusion: { in: Status.const_values }
  validates :repeatable, inclusion: { in: [false] }
  validates :timezone, inclusion: { in: CommonConst::ORIGIN_TIMEZONES }
  validates :invited_participants, presence: true

  validate :validate_timer_is_reasonable, :validate_idea_is_non_archived

  def singleton_idea
    ideas.first unless repeatable
  end

  private

  def validate_timer_is_reasonable
    return if repeatable || !timezone

    unless timer.is_a?(Hash) && timer[:start_time].match?(CommonConst::IdeaioRegexp::DATETIME_WITHOUT_TZ) &&
           timer[:release_time].match?(CommonConst::IdeaioRegexp::DATETIME_WITHOUT_TZ)

      return errors.add(:timer, "can't lack start_time and release_time")
    end

    start_time = Time.find_zone(timezone).parse(timer[:start_time])
    if start_time < START_IDEA_FREEZE_MINUTES.minutes.after
      errors.add(:timer, "can't modify the upcoming idea (start_time < #{START_IDEA_FREEZE_MINUTES}.minute.after)")
    end

    release_time = Time.find_zone(timezone).parse(timer[:release_time])
    if release_time - start_time < MIN_IDEA_DURATION_MINUTES.minutes # rubocop:disable Style/GuardClause
      errors.add(:timer, "idea duration is too short (< #{MIN_IDEA_DURATION_MINUTES}.minutes)")
    end
  end

  def validate_idea_is_non_archived
    return unless archived_at && changes.except(:archived_at).present?

    errors.add(:archived_at, 'archived idea_definition cannot be modified')
  end

  def setup_new_idea_job
    StartNewIdeaJob.set(wait_until: Time.find_zone(timezone).parse(timer['start_time'])).perform_later(id)
  end
end
