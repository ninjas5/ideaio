# frozen_string_literal: true

class User < ApplicationRecord
  include Archiveable

  belongs_to :workspace

  has_one :slack_user
  has_one :notion_connection

  has_many :idea_templates
  has_many :idea_definitions
  has_many :idea_participants
  has_many :answer_comments

  has_many :access_grants,
    class_name: 'Doorkeeper::AccessGrant',
    foreign_key: :resource_owner_id,
    dependent: :delete_all # or :destroy if you need callbacks

  has_many :access_tokens,
    class_name: 'Doorkeeper::AccessToken',
    foreign_key: :resource_owner_id,
    dependent: :delete_all # or :destroy if you need callbacks

  has_many :active_access_tokens,
    -> { where(revoked_at: nil) },
    class_name: 'Doorkeeper::AccessToken',
    foreign_key: :resource_owner_id

  delegate :slack_team, to: :slack_user

  module Role
    extend ConstantValue

    ADMIN = 'admin'
    BASIC = 'basic'
  end

  def archive!
    return if archived?

    transaction do
      update!(archived_at: Time.zone.now)
      DeleteArchivedUserJob.set(wait_until: CommonConst::ACCOUNT_DELETION_BUFFER.after).perform_later(id)
    end
  end
end
