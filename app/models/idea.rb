# frozen_string_literal: true

class Idea < ApplicationRecord
  belongs_to :workspace
  belongs_to :idea_definition

  has_many :questions, class_name: 'IdeaQuestion'
  has_many :idea_participants

  serialize :invited_participants, Serializers::IndifferentAccessJsonbSerializer

  scope :filter_participant, lambda { |category: nil, ref_team_id: nil, ref_user_id: nil|
    condition = {}
    condition[:category] = category if category.present?
    condition[:ref_team_id] = ref_team_id if ref_team_id.present?
    condition[:ref_user_id] = ref_user_id if ref_user_id.present?
    where 'ideas.invited_participants @> ?', [condition].to_json
  }

  scope :filter_participant_by_user, lambda { |user|
    filter_participant(
      category: user.workspace.category,
      ref_team_id: user.slack_user.slack_team.ref_team_id,
      ref_user_id: user.slack_user.ref_user_id,
    )
  }

  scope :report_shareable, -> { where report_shareable: true }

  module Status
    extend ConstantValue

    ENABLED = 'enabled'
    DISABLED = 'disabled'
  end

  module RunningStatus
    extend ConstantValue

    NOT_STARTED = 'not_started' # 此状态不存在于数据库中
    STARTED = 'in_progress'
    RELEASED = 'released'
  end

  after_create :setup_release_idea_job

  private

  def setup_release_idea_job
    ReleaseIdeaJob.set(wait_until: release_time).perform_later(id)
  rescue StandardError => e
    logger.error(e)
  end
end
