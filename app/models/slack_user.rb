# frozen_string_literal: true

class SlackUser < ApplicationRecord
  belongs_to :workspace
  belongs_to :slack_team
  belongs_to :user
end
