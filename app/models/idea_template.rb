# frozen_string_literal: true

class IdeaTemplate < ApplicationRecord
  belongs_to :workspace, optional: true
  belongs_to :user, optional: true

  has_many :questions, class_name: 'IdeaTemplateQuestion'
  has_many :idea_definitions

  module Category
    extend ConstantValue

    GENERAL = 'General'
    ONE_ON_ONE = 'One-on-one'
    ENGINEERING = 'Engineering'
    CS = 'Customer success'
    MARKETING = 'Marketing'
  end

  module Status
    extend ConstantValue

    ENABLED = 'enabled'
    DISABLED = 'disabled'
  end

  scope :enabled, -> { where status: Status::ENABLED }
end
