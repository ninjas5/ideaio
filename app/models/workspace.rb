# frozen_string_literal: true

class Workspace < ApplicationRecord
  include Archiveable

  has_one :slack_team
  has_many :slack_users

  has_many :users
  has_many :idea_templates
  has_many :idea_definitions
  has_many :ideas
  has_many :idea_participants
  has_many :question_answers
  has_many :answer_comments

  module Category
    extend ConstantValue
    SLACK = 'slack'
  end

  delegate :slack_client, :refresh_access_token, to: :slack_team
end
