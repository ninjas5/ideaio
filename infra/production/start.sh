#!/bin/bash

# Start Nginx
/etc/init.d/nginx start

# Run database migration
bundle exec rails db:migrate

# Run delayed job
bin/delayed_job -n 2 start

# Start Rails
bundle exec rails s
