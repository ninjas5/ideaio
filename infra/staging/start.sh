#!/bin/bash

function start_nginx() {
    log_info "Start nginx, ... ..."
    /etc/init.d/nginx start && log_info "Start nginx successful"
}

function start_database() {
    log_info "Start database server, ... ..."
    sed -Ei 's/(local +all +postgres +)[a-z]*$/\1trust/g' /etc/postgresql/*/main/pg_hba.conf && \
        sed -Ei 's/(local +all +all +)[a-z]$/\1md5/g' /etc/postgresql/*/main/pg_hba.conf && \
        /etc/init.d/postgresql start && \
        psql -U postgres -c "ALTER USER postgres with password 'postgres';" && \
        log_info "Start database server successful"
}

function init_database() {
    log_info "Init database, start ... ..."

    bundle exec rails db:create && \
        log_info "Create database \"ideaio_staging\" successful" && \
        bundle exec rails db:migrate && \
        log_info "Run database migration successful" && \
        bundle exec bundle exec rails backfills:dev:reset_db_data_in_dev_env && \
        log_info "Setup init data successful"
        bundle exec bundle exec rails backfills:reset_idea_templates &&\
        log_info "Reset idea templates successful"
}

function start_delayed_job() {
    log_info "Start delayed job ... ..."
    bin/delayed_job -n 2 start
}

function start_rails() {
    log_info "Start rails service ... ..."
    bundle exec rails s
}

function log_info() {
    echo "`date +'%Y-%m-%d %H:%M:%S'`:INFO:$*"
}

function main() {
    start_nginx && start_delayed_job && start_rails
}

main $@
