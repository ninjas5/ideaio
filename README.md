# Ideaio

项目描述: `TODO: @emma.guo`

## [Docs](doc/)

- [Development](doc/Development.md)
- [Docker](doc/Docker.md)

## Getting Started

```bash
# Install Ruby
rvm install 2.7.4
rvm use 2.7.4
gem install bundler

# Clone Code
git clone <git_repo>
cd ideaio

# Install gems
bundle install

# Db migration
rails db:create
rails db:migrate
rails backfills:dev:reset_db_data_in_dev_env
# rails db:seed

# Run in dev
rails s

# Run unit test
rspec spec

# Generate swagger API doc
rails rswag:specs:swaggerize
```
