# frozen_string_literal: true

module Backfills
  class ResetIdeaTemplate
    def run
      yamls = YAML.safe_load(ERB.new(File.read(Rails.root.join('data/idea_templates.yml'))).result)['idea_templates']
      yamls ||= []
      existing_template_ids = IdeaTemplate.enabled.ids

      IdeaTemplate.transaction do
        yamls.each do |yaml|
          template = IdeaTemplate.find(yaml['id']) if yaml['id'].present?
          template ||= IdeaTemplate.find_by(name: yaml['name'])
          template ||= IdeaTemplate.new(name: yaml['name'])
          template_params = yaml.slice('category', 'name', 'description').merge(status: IdeaTemplate::Status::ENABLED)
          template.update! template_params
          existing_template_ids.delete(template.id)

          template.questions.destroy_all
          question_params = yaml['questions'].each_with_index.map { |temp, index| temp.merge('ordinal' => index) }
          template.questions.create! question_params
        end

        # Disable other idea templates
        IdeaTemplate.where(id: existing_template_ids).update(status: IdeaTemplate::Status::DISABLED)
      end
    end
  end
end

namespace :backfills do
  desc 'Reset idea templates'
  task reset_idea_templates: :environment do
    Backfills::ResetIdeaTemplate.new.run

    Rails.logger.info 'Reset idea templates'
  end
end
