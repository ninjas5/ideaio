# frozen_string_literal: true

module Backfills
  module Dev
    class ResetDbDataInDevEnv
      def run
        ApplicationRecord.transaction do
          cleanup

          init_data
        end
      end

      private

      def cleanup
        AnswerComment.destroy_all
        QuestionAnswer.destroy_all
        IdeaParticipant.destroy_all
        NotionConnection.destroy_all

        IdeaQuestion.destroy_all
        Idea.destroy_all

        IdeaQuestionDefinition.destroy_all
        IdeaDefinition.destroy_all

        IdeaTemplateQuestion.destroy_all
        IdeaTemplate.destroy_all

        SlackUser.destroy_all
        SlackTeam.destroy_all

        Doorkeeper::AccessGrant.destroy_all
        Doorkeeper::AccessToken.destroy_all
        # Doorkeeper::Application.destroy_all
        User.destroy_all
        Workspace.destroy_all
      end

      def init_data
        application = Doorkeeper::Application.find_by!(name: CommonConst::OAuthApplication::IDEAIO)

        workspace = Workspace.create!(category: Workspace::Category::SLACK, name: 'test')
        sky, = users = workspace.users.create!([
          { email: 'sky@test.com', roles: [User::Role::ADMIN, User::Role::BASIC] },
          { email: 'emma@test.com', roles: [User::Role::ADMIN, User::Role::BASIC] },
          { email: 'test@test.com', roles: [User::Role::BASIC] },
        ])
        users.each do |user|
          user.access_grants.create!(application: application, expires_in: 10.years.ago,
            redirect_uri: application.redirect_uri, scopes: application.scopes)
          user.access_tokens.create!(application: application, scopes: application.scopes, use_refresh_token: true)
        end

        slack_team = workspace.create_slack_team!(
          ref_team_id: 'slack_team_test_1',
          ref_team_name: workspace.name,
          scope: '',
          access_token: "#{SecureRandom.random_number(100)}-access-token",
          refresh_token: "#{SecureRandom.random_number(100)}-refresh-token",
          expires_in: 3600,
        )

        slack_users_params = users.map do |user|
          {
            workspace: slack_team.workspace,
            user: user,
            ref_user_id: "slack_user_id_#{user.id}",
            email: user.email,
            scope: '',
            token_type: 'user',
            access_token: "access_token_#{user.id}",
            refresh_token: "refresh_token_#{user.id}",
            expires_in: 3600,
            profile: {
              avatar_hash: user.email.hash.to_s,
              email: user.email,
              real_name: user.email,
            },
          }
        end
        slack_team.slack_users.create!(slack_users_params)

        # Global Template - Daily standup
        daily_standup = IdeaTemplate.create!(
          category: IdeaTemplate::Category::GENERAL,
          name: 'Daily standup',
          description: 'daily standup',
          status: IdeaTemplate::Status::ENABLED,
        )
        daily_standup.questions.create!([
          { ordinal: 0, required: true, content: '昨天完成了什么？' },
          { ordinal: 1, required: true, content: '今天计划做什么？' },
          { ordinal: 2, required: true, content: '有什么被block了么？' },
        ])

        # Global Template - Sprint retro
        retro = IdeaTemplate.create!(
          category: IdeaTemplate::Category::GENERAL,
          name: 'Retrospective',
          description: 'Sprint Retrospective',
          status: IdeaTemplate::Status::ENABLED,
        )
        retro.questions.create!([
          { ordinal: 0, required: true, content: '有哪些做得好的？' },
          { ordinal: 1, required: true, content: '有哪些需要改善的？' },
          { ordinal: 2, required: true, content: '工作量多少？' },
        ])

        # Idea definition
        timezone = CommonConst::ORIGIN_TIMEZONES.first
        daily_standup_definition = daily_standup.idea_definitions.create!(
          workspace: workspace,
          user: sky,
          name: daily_standup.name,
          description: daily_standup.description,
          status: IdeaDefinition::Status::ENABLED,
          repeatable: false,
          timezone: timezone,
          timer: {
            start_time: 1.hour.after.in_time_zone(timezone).strftime('%Y-%m-%d %H:%M:%S'),
            release_time: 2.hours.after.in_time_zone(timezone).strftime('%Y-%m-%d %H:%M:%S'),
          },
          channels: ['general'],
          invited_participants: sky.workspace.slack_users.map do |slack_user|
            {
              category: sky.workspace.category,
              ref_team_id: slack_user.slack_team.ref_team_id,
              ref_user_id: slack_user.ref_user_id,
            }
          end,
          report_shareable: true,
          anonymous: true,
        )

        # Create idea definition questions based on template question content
        daily_standup_definition.questions.create!(
          daily_standup.questions.map do |question|
            question.slice(:ordinal, :required, :content)
          end,
        )

        # Create idea questions based on idea definition question content
        keys = %i[workspace name description timezone anonymous invited_participants report_shareable status]

        idea = daily_standup_definition.ideas.create!(
          daily_standup_definition.slice(*keys)
            .merge(
              start_time: Time.zone.parse('2022-04-09 09:00:00'),
              release_time: Time.zone.parse('2022-04-09 09:30:00'),
              running_status: Idea::RunningStatus::RELEASED,
            ),
        )

        idea.questions.create!(
          daily_standup_definition.questions.map do |question|
            question.slice(:ordinal, :required, :content)
          end,
        )

        idea_participants = idea.idea_participants.create!(
          idea.invited_participants.map do |participant|
            temp_team = SlackTeam.find_by!(ref_team_id: participant[:ref_team_id])
            slack_user = temp_team.slack_users.find_by!(ref_user_id: participant[:ref_user_id])

            {
              workspace: temp_team.workspace,
              user: slack_user.user,
            }
          end,
        )

        idea_participants.each do |idea_participant|
          idea_participant.question_answers.create!(
            idea_participant.idea.questions.map do |question|
              {
                workspace: idea_participant.workspace,
                idea_question: question,
                content: "Answer-#{question.ordinal}, #{question.content}",
                tags: [],
                attachments: [],
                vote_user_ids: [sky.id],
              }
            end,
          )
        end
      end
    end
  end
end

namespace :backfills do
  namespace :dev do
    desc 'Clean and init metadata'
    task reset_db_data_in_dev_env: :environment do
      Backfills::Dev::ResetDbDataInDevEnv.new.run

      Rails.logger.info 'Clean and init metadata'
    end
  end
end
