# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby(File.read(File.expand_path('.ruby-version', __dir__)))

gem 'bootsnap', require: false
gem 'bulk_insert'
gem 'jbuilder', '~> 2.7'
gem 'puma'
gem 'rack-cors'
gem 'rack-utf8_sanitizer'
gem 'rails', '~> 6.1'
gem 'sass-rails', '>= 6'
gem 'turbolinks', '~> 5'
gem 'webpacker', '~> 5.0'

# Logger
gem 'amazing_print'
gem 'rails_semantic_logger'

# Database
gem 'active_record_union'
gem 'pg', '~> 1.1'

# Job
gem 'daemons'
gem 'delayed_job_active_record'

# Cache
# gem "redis", "~> 4.0"
# gem "kredis"

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
# gem "rack-cors"

group :development do
  gem 'listen'
  gem 'web-console'
end

group :development, :test do
  gem 'dotenv-rails'

  gem 'byebug'
  gem 'spring'

  gem 'database_cleaner'
  gem 'factory_bot_rails'
  gem 'rspec-rails'
  gem 'simplecov'

  gem 'rswag-specs'

  # Integration Test
  # gem 'capybara', '>= 2.15'
  # gem 'selenium-webdriver'
  # gem 'webdrivers'

  gem 'brakeman'
  gem 'rails_best_practices'
  gem 'rubocop'
  gem 'rubocop-packaging'
  gem 'rubocop-performance'
  gem 'rubocop-rails'
  gem 'rubocop-rspec'
  gem 'rubycritic'

  gem 'ruby-prof'

  gem 'ffaker'
end

# Swagger API
gem 'rswag-api'
gem 'rswag-ui'

# OAuth
gem 'bcrypt'
gem 'doorkeeper'
gem 'doorkeeper-jwt'

# # User and permissions
# gem 'cancancan'
# gem 'devise'
# gem 'devise-encryptable'
# gem 'ffi', '1.15.3'

# # Active Admin
# gem 'activeadmin', github: 'june-sky/activeadmin'
# gem 'activeadmin_addons'

# Slack
gem 'slack-ruby-bot-server'
gem 'slack-ruby-client'

# Notion
gem 'notion-ruby-client'

# Sentry
gem 'sentry-rails'
gem 'sentry-ruby'
